package Models;

/**
 * Created by Admin on 27.04.2018.
 */

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table( name = "USERS" )
public class User implements Serializable {
    private Long id;

    private String name;
    private String username;
    private String password;
    private boolean isAdmin;
    private int accountNumber;



    public User() {
        // this form used by Hibernate
    }

    public User(String name,String username,String password,boolean isAdmin,int accountNumber) {
        // for application use, to create new events
        this.name = name;
        this.username=username;
        this.password=password;
        this.isAdmin=isAdmin;
        this.accountNumber=accountNumber;
    }

    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    public Long getId() {
        return id;
    }

    private void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }
}
