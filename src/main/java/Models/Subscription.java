package Models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Created by Admin on 31.05.2018.
 */
@Entity
@Table( name = "SUBS" )
public class Subscription implements Serializable {
    private Long id;
    private String username;
    private boolean type;
    private int limitedAds;
    private LocalDateTime lastTimePosted;

    public Subscription( String username, boolean type, int limitedAds, LocalDateTime lastTimePosted) {
        this.username = username;
        this.type = type;
        this.limitedAds = limitedAds;
        this.lastTimePosted = lastTimePosted;
    }

    public Subscription(){

    }
    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isType() {
        return type;
    }

    public void setType(boolean type) {
        this.type = type;
    }

    public int getLimitedAds() {
        return limitedAds;
    }

    public void setLimitedAds(int limitedAds) {
        this.limitedAds = limitedAds;
    }

    public LocalDateTime getLastTimePosted() {
        return lastTimePosted;
    }

    public void setLastTimePosted(LocalDateTime lastTimePosted) {
        this.lastTimePosted = lastTimePosted;
    }
}
