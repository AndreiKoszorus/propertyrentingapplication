package Models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by Admin on 22.05.2018.
 */
@Entity
@Table( name = "MESSAGES" )
public class Message implements Serializable     {

    private Long id;
    private String toUsername;
    private String fromUsername;
    private String text;

    public Message (String toUsername,String fromUsername,String text){
        this.toUsername=toUsername;
        this.fromUsername=fromUsername;
        this.text=text;
    }

    public Message(){

    }
    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    public Long getId() {
        return id;
    }

    public String getFromUsername() {
        return fromUsername;
    }

    public String getText() {
        return text;
    }

    public String getToUsername() {
        return toUsername;
    }

    public void setFromUsername(String fromUsername) {
        this.fromUsername = fromUsername;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setToUsername(String toUsername) {
        this.toUsername = toUsername;
    }
}
