package Models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;


/**
 * Created by Admin on 02.05.2018.
 */
@Entity
@Table( name = "ADS" )
public class Ad implements Serializable {
    private Long id;
    private String owner;
    private String location;
    private double price;
    private boolean available;

    public Ad(){

    }

    public Ad(String owner,String location,double price,boolean available){
        this.owner=owner;
        this.location=location;
        this.price=price;
        this.available=available;
    }
    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    public Long getId() {
        return id;
    }

    public String getOwner() {
        return owner;
    }

    public String getLocation() {
        return location;
    }

    public double getPrice() {
        return price;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }
}
