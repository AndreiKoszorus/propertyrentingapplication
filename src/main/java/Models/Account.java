package Models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Admin on 02.05.2018.
 */
@Entity
@Table( name = "ACCOUNTS" )
public class Account {
    private Long id;
    private int accountNumber;
    private String expirationDate;
    private double amount;

    public Account(){

    }

    public Account(int accountNumber,String expirationDate,double amount){
        this.accountNumber=accountNumber;
        this.expirationDate=expirationDate;
        this.amount=amount;
    }
    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    public Long getId() {
        return id;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
