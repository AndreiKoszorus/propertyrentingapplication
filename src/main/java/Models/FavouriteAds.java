package Models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by Admin on 15.05.2018.
 */
@Entity
@Table( name = "FAVOURITE" )
public class FavouriteAds implements Serializable {
    private Long id;
    private Long adId;
    private String username;

    public FavouriteAds(){

    }
    public FavouriteAds(Long adId,String username){
        this.adId=adId;
        this.username=username;
    }
    @Id
    @GeneratedValue(generator="increment")
    @GenericGenerator(name="increment", strategy = "increment")
    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public Long getAdId() {
        return adId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setAdId(Long adId) {
        this.adId = adId;
    }
}
