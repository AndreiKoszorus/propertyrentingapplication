package Request;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Admin on 16.05.2018.
 */
public class Request implements Serializable {
    private String head;
    private Object body;

    public Request(String head,Object body){
        this.head=head;
        this.body=body;
    }

    public Request(){

    }

    public Object getBody() {
        return body;
    }

    public String getHead() {
        return head;
    }

    public void setBody(Object body) {
        this.body = body;
    }

    public void setHead(String head) {
        this.head = head;
    }
}
