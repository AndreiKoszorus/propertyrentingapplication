package Client.GenerateReport;

import Models.Ad;

import java.io.PrintWriter;
import java.util.List;

/**
 * Created by Admin on 03.05.2018.
 */
public class GenerateTXT implements Files {

    @Override
    public void generateFile(String path, List<Ad> ads) {
        try {
            PrintWriter writer = new PrintWriter(path, "UTF-8");
            StringBuilder stringBuilder=new StringBuilder();
            for(Ad ad:ads){
                writer.print(ad.getId());
                writer.print("   ");
                writer.print(ad.getOwner());
                writer.print("   ");
                writer.print(ad.getLocation());
                writer.print("   ");
                writer.print(ad.isAvailable());
                writer.println();

            }
            writer.close();
        }
        catch(Exception e){
            e.getMessage();
        }
    }
}
