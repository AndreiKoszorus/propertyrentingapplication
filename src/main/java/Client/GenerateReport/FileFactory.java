package Client.GenerateReport;

/**
 * Created by Admin on 03.05.2018.
 */
public class FileFactory {

    public static Files generateFiles(String fileType){
        if(fileType.compareTo("PDF")==0){
            return new GeneratePDF();
        }
        else if(fileType.compareTo("TXT")==0){
            return new GenerateTXT();
        }

        return null;
    }
}
