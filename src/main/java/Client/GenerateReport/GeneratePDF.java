package Client.GenerateReport;

import Models.Ad;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;
import java.util.List;

/**
 * Created by Admin on 03.05.2018.
 */
public class GeneratePDF implements Files {

    private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,
            Font.BOLD);
    private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16,
            Font.BOLD);
    private static Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12,
            Font.BOLD);
    @Override
    public void generateFile(String path, List<Ad> ads) {
        try {
            PdfPTable table=new PdfPTable(5);
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(path));
            document.open();
            document.addTitle("Users ads");
            Paragraph preface = new Paragraph();

            preface.add(new Paragraph(" "));

            preface.add(new Paragraph("All users ads ", catFont));

            preface.add(new Paragraph(" "));
            preface.add(new Paragraph(" "));
            preface.add(new Paragraph(" "));

            table.setWidthPercentage(100);
            PdfPCell c1 = new PdfPCell(new Phrase("Id"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("Owner"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("Location"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
            table.setHeaderRows(1);

            c1 = new PdfPCell(new Phrase("Price"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
            table.setHeaderRows(1);

            c1 = new PdfPCell(new Phrase("Available"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
            table.setHeaderRows(1);

            for(Ad ad: ads){
                String id=""+ad.getId();
                String price=""+ad.getPrice();
                String available=""+ad.isAvailable();
                table.addCell(id);
                table.addCell(ad.getOwner());
                table.addCell(ad.getLocation());
                table.addCell(price);
                table.addCell(available);
            }
            document.add(preface);
            document.add(table);

            document.close();

        }
        catch (Exception e){
            e.getMessage();
        }
    }
}
