package Client.GenerateReport;

import Models.Ad;

import java.util.List;

/**
 * Created by Admin on 03.05.2018.
 */
public interface Files {
    public void generateFile(String path,List<Ad> ads);
}
