package Client;

import Client.GUI.*;
import Models.Ad;
import Models.Message;
import Models.User;
import Request.Request;
import Server.Server;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 17.05.2018.
 */
public class Client {

    public static class ClientConnection extends Thread{
        private final Socket socket;
        private final ObjectOutputStream output;
        private final ObjectInputStream input;
        private LoginControllerOP loginControllerOP;
        private AdminController adminControllerOP;
        private RegularUserControllerOP regulatUserControllerOP;
        private Request sendRequest;
        private static ArrayList<String> clientsUsernames;

        public ClientConnection(Socket socket,LoginControllerOP loginControllerOP) throws IOException{
            this.socket=socket;
            output=new ObjectOutputStream(socket.getOutputStream());
            input=new ObjectInputStream(socket.getInputStream());
            this.loginControllerOP=loginControllerOP;
        }

        public void run(){
            try{
            while(socket.isConnected()){
                boolean serverHasData=socket.getInputStream().available()>0;
                if(serverHasData) {

                    Request request = (Request) input.readObject();
                    sendRequest=request;

                    if (request.getHead() != null) {
                        switch (request.getHead()) {
                            case "ADMIN":
                                loginControllerOP.showAdmin();
                                break;
                            case "USER":
                                loginControllerOP.showUser();
                                break;
                            case "ADS-TO-DISPLAY":
                                List<Ad> ads = (List<Ad>) request.getBody();
                                if(ads!=null)
                                adminControllerOP.displayRequestedAds(ads);
                                break;
                            case "DISPLAY-THE-USERS":

                                List<User> users=(List<User>) request.getBody();
                                if(users!=null)
                                adminControllerOP.displayRequestedUsers(users);
                                break;
                            case "ADS-FOR-FILE":
                                List<Ad> adsForGenerate=(List<Ad>) request.getBody();
                                adminControllerOP.generate(adsForGenerate);
                                break;
                            case "DISPLAY-ALL-ADS":
                                List<Ad> adsForDisplay=(List<Ad>) request.getBody();
                                if(adsForDisplay!=null)
                                regulatUserControllerOP.displayRequestedAds(adsForDisplay);
                                break;
                            case "DISPLAY-LOCATION-FILTER":
                                List<Ad> adsForLocation=(List<Ad>) request.getBody();
                                if(adsForLocation!=null)
                                regulatUserControllerOP.displayRequestedAds(adsForLocation);
                                break;
                            case "DISPLAY-PRICE-FILTER":
                                List<Ad> adsForPrice=(List<Ad>) request.getBody();
                                if(adsForPrice!=null)
                                regulatUserControllerOP.displayRequestedAds(adsForPrice);
                                break;
                            case "MY-ADS":
                                List<Ad> myAds=(List<Ad>) request.getBody();
                                regulatUserControllerOP.displayRequestedAds(myAds);
                                break;
                            case "DISPLAY-FAV":
                                List<Ad> favAds=(List<Ad>) request.getBody();
                                if(favAds!=null) {
                                    regulatUserControllerOP.displayRequestedAds(favAds);
                                }
                                break;
                            case "DISPLAY-REQUESTED-ADS":
                                List<Message> messages=(List<Message>) request.getBody();
                                if(messages!=null) {
                                    regulatUserControllerOP.displayRequestedMessages(messages);
                                }
                                break;
                            case "NOTIFY":
                                String toUsername=(String) request.getBody();

                                String currentUsername=regulatUserControllerOP.getCurrentUser();
                                if(toUsername.equals(currentUsername)){
                                    regulatUserControllerOP.notifyUser();
                                }
                                break;
                            case "DISPLAY-RATING-FILTER":
                                List<Ad> ads1=(List<Ad>)request.getBody();
                                if(ads1!=null){
                                    regulatUserControllerOP.displayRequestedAds(ads1);
                                }
                        }
                    }
                }

                try{
                    Thread.sleep(500);
                }
                catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
        }
        catch (IOException | ClassNotFoundException e){
            e.printStackTrace();
        }

    }

    public void sendMessageToServer(Request request) throws IOException{
        output.writeObject(request);
    }

        public void setAdminControllerOP(AdminController adminControllerOP) {
            this.adminControllerOP = adminControllerOP;
        }

        public void setRegularUserControllerOP(RegularUserControllerOP regularUserControllerOP){
            this.regulatUserControllerOP=regularUserControllerOP;
        }

        public Request getRequest() {
            return sendRequest;
        }
    }

}
