package Client.GUI;

import Models.Ad;
import Models.Ratings;
import Models.Subscription;
import Models.User;
import Server.BL.*;
import Server.DAO.*;
import org.mockito.cglib.core.Local;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.Date;
import java.util.List;

/**
 * Created by Admin on 09.05.2018.
 */
public class Run {
    public static void main(String []args) throws IOException,ClassNotFoundException{

        //clientConnection.start();
        AdminView adminController=new AdminView();
        FavouriteAdDAO favouriteAdDAO=new FavouriteAdDAO();
        Long id=new Long(2);
        UserDAO userDAO=new UserDAO();
        User user= new User("Koszorus Andrei","koszorus.andrei@gmail.com","12345",true,544331);
        RatingDAO ratingDAO=new RatingDAO();
        AdDAO adDAO = new AdDAO();

        //userDAO.createUser(user);
        //favouriteAdDAO.deleteFavouriteAd(id,"george.farcas@gmail.com");
        Validators validators=new Validators(new AdDAO(),new UserDAO(),favouriteAdDAO,new MessageDAO(),ratingDAO);
        RatingBLL ratingBLL=new RatingBLL(ratingDAO,validators);
        SubDAO subDAO=new SubDAO();
        Subscription subscription=subDAO.getSubscription("george.farcas@gmail.com");

        //userDAO.createUser(user);

        //RegularUser regularUser=new RegularUser(adDAO,validators,new FavouriteAdBLL(favouriteAdDAO,adDAO,validators),ratingBLL);

        LocalDateTime localDateTime=LocalDateTime.now();
        LocalDateTime localDateTime1=localDateTime.minusSeconds(1);
       Duration duration=Duration.between(subscription.getLastTimePosted(),localDateTime);

        System.out.print(duration.getSeconds()/60);
        //regularUser.searchByRate(5);
        //List<Ratings> ratingsList=ratingDAO.findAdByRate(4);
        //System.out.print(ratingsList.size());
        //ratingBLL.createRating(new Ratings("george.farcas@gmail.com",id,5));
        /*try {
            validators.checkFavAdExistance(id,"george.farcas@gmail.com");
        } catch (ExceptionHendler exceptionHendler) {
            exceptionHendler.printStackTrace();
        }*/
        //LoginViewOp loginView=new LoginView();
        //loginView.setAdminControllerOP(adminController);
        //loginView.startApplication();



        //LoginView loginController=new LoginView();
        //loginController.startApplication();
        //clientConnection.sendMessageToServer("dada");
    }
}
