package Client.GUI;

import Models.Ad;
import Models.Message;
import Request.Request;

import java.io.IOException;
import java.util.List;

/**
 * Created by Admin on 21.05.2018.
 */
public interface RegularUserControllerOP {

    public void createAd() throws IOException;
    public void updateAd() throws IOException;
    public void setCurrentUser(String currentUser);
    public void deleteAd() throws IOException;
    public void displayRequestedAds(List<Ad> ads);
    public void displayAds() throws IOException;
    public void displayMyAds() throws IOException;
    public void createFavouriteAd() throws IOException;
    public void displayFavouriteAds() throws IOException;
    public void createMessage() throws IOException;
    public void displayRequestedMessages(List<Message> messages);
    public void displayMessage() throws IOException;
    public String getCurrentUser();
    public void notifyUser();
    public void deleteFavAds() throws IOException;
    public void createRating() throws IOException;


}
