package Client.GUI;

import Client.Client;

import java.io.IOException;

/**
 * Created by Admin on 17.05.2018.
 */
public interface LoginControllerOP {
    public void showAdmin() throws IOException;
    public void showUser() throws IOException;
    public void openWindow() throws IOException;
    public String getUsernameToSend();
}
