package Client.GUI;

import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.io.IOException;

/**
 * Created by Admin on 04.05.2018.
 */
public class RegularUserView implements RegularUserViewOP {
    private String username;
    @FXML
    private TextField testField;
    @FXML
    private TextField createLocation;
    @FXML
    private TextField createPrice;
    @FXML
    private ComboBox createAvailable;
    @FXML
    private TextField idToUpdate;
    @FXML
    private TextField updateLocation;
    @FXML
    private TextField updatePrice;
    @FXML
    private ComboBox updateAvailable;
    @FXML
    private TextField adNrDelete;
    @FXML
    private TextArea displayAds;
    @FXML
    private ComboBox filterBy;
    @FXML
    private TextField displayLocation;
    @FXML
    private TextField displayPrice;
    @FXML
    private ComboBox displayAvailable;
    @FXML
    private TextField favouriteAdNr;
    @FXML
    private TextArea messageField;
    @FXML
    private TextField notificationField;
    @FXML
    private TextArea inboxField;
    @FXML
    private TextField chatUsername;
    @FXML
    private TextArea messageFields;
    @FXML
    private TextField removeFavouriteAds;
    @FXML
    private ComboBox rateAds;
    @FXML
    private TextField rateId;
    @FXML
    private ComboBox rateAdsFilter;


    private RegularUserControllerOP regularUserControllerOP;

    public String getUsername() {
        return username;
    }

    public TextField getTestField() {
        return testField;
    }

    public TextField getCreateLocation() {
        return createLocation;
    }

    public TextField getCreatePrice() {
        return createPrice;
    }

    public ComboBox getCreateAvailable() {
        return createAvailable;
    }

    public TextField getIdToUpdate() {
        return idToUpdate;
    }

    public TextField getUpdateLocation() {
        return updateLocation;
    }

    public TextField getUpdatePrice() {
        return updatePrice;
    }

    public ComboBox getUpdateAvailable() {
        return updateAvailable;
    }

    public TextField getAdNrDelete() {
        return adNrDelete;
    }

    public TextArea getDisplayAds() {
        return displayAds;
    }

    public ComboBox getFilterBy() {
        return filterBy;
    }

    public TextField getDisplayLocation() {
        return displayLocation;
    }

    public TextField getDisplayPrice() {
        return displayPrice;
    }

    public ComboBox getDisplayAvailable() {
        return displayAvailable;
    }

    public TextField getFavouriteAdNr() {
        return favouriteAdNr;
    }

    public TextArea getMessageField() {
        return messageField;
    }

    public TextField getNotificationField() {
        return notificationField;
    }

    public TextArea getMessageFields() {
        return messageFields;
    }

    public TextField getChatUsername() {
        return chatUsername;
    }

    public TextField getRemoveFavouriteAds() {
        return removeFavouriteAds;
    }

    public ComboBox getRateAds() {
        return rateAds;
    }

    public TextField getRateId() {
        return rateId;
    }

    public ComboBox getRateAdsFilter() {
        return rateAdsFilter;
    }

    public void setDisplayAds(String displayAds) {
        this.displayAds.setText(displayAds);
    }

    public void setInboxField(String message){
        this.inboxField.setText(message);
    }

    public void setNotificationField(String notificationField) {
        this.notificationField.setText(notificationField);
    }

    public void setRegulatUserControllerOP(RegularUserControllerOP regularUserControllerOP){
        this.regularUserControllerOP=regularUserControllerOP;
    }

    public void createAd() throws IOException{
        regularUserControllerOP.createAd();

    }

    public void updateAd() throws IOException{
       regularUserControllerOP.updateAd();

    }

    public void deleteAd() throws IOException{
        regularUserControllerOP.deleteAd();
    }

    public void displayMyAds() throws IOException{

        regularUserControllerOP.displayMyAds();

    }

    public void displayAds()throws IOException{
        regularUserControllerOP.displayAds();

    }

    public void createFavouriteAd() throws IOException{

        regularUserControllerOP.createFavouriteAd();
    }

    public void displayFavouriteAds() throws IOException{
       regularUserControllerOP.displayFavouriteAds();
    }

    public void sendMessage() throws IOException{
        regularUserControllerOP.createMessage();
    }

    public void displayMessages() throws IOException{
        regularUserControllerOP.displayMessage();
    }

    public void deleteFavAds() throws IOException{
        regularUserControllerOP.deleteFavAds();
    }

    public void createRating() throws IOException{
        regularUserControllerOP.createRating();
    }

}
