package Client.GUI;

import Client.Client;
import Request.Request;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.Socket;

/**
 * Created by Admin on 03.05.2018.
 */
public class LoginView extends Application implements LoginViewOp {
    private static Stage primaryStage;
    public static Client.ClientConnection  clientConnection;
    public static String usernameToSend;
    @FXML
    private PasswordField password;
    @FXML
    private TextField textField;
    @FXML
    private Button loginButton;
    private LoginControllerOP loginControllerOP;
    private CreateView createView;





    public void startApplication(){
        Application.launch();
    }
    @Override
    public void start(Stage stage) throws Exception {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                try {
                    primaryStage = stage;
                    FXMLLoader loader = new FXMLLoader();
                    // Path to the FXML File
                    String fxmlDocPath = "E:/Faculta/ASSIGMENT2/assigment2/src/main/java/Client/GUI/LoginWindow.fxml";
                    FileInputStream fxmlStream = new FileInputStream(fxmlDocPath);
                    //System.out.println(username);

                    // Create the Pane and all Details
                    AnchorPane root = (AnchorPane) loader.load(fxmlStream);
                    // Create the Scene
                    Scene scene = new Scene(root);
                    // Set the Scene to the Stage
                    stage.setScene(scene);
                    // Set the Title to the Stage
                    stage.setTitle("Login");
                    // Display the Stage
                    stage.show();
                }
                catch (IOException e){
                    e.printStackTrace();
                }



            }
        });

    }

    public PasswordField getPassword() {
        return password;
    }


    public TextField getTextField() {
        return textField;
    }


    public void openWindow() throws IOException{
        String username = textField.getText();
        usernameToSend = username;
       LoginController loginController = new LoginController(this);
       loginController.openWindow();
    }


    public void showAdminWindow(CreateView createView) throws IOException{
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                try {

                    AnchorPane root = (AnchorPane) createView.getAdminLoader();
                    Scene scene = new Scene(root);
                    Stage stage = new Stage();
                    stage.initModality(Modality.WINDOW_MODAL);
                    stage.initOwner(primaryStage);
                    stage.setScene(scene);
                    stage.showAndWait();
                }
                catch (IOException e){
                    e.printStackTrace();
                }

            }
        });

    }

    public void showUserWindow(CreateView createView) throws IOException{
        Platform.runLater(new Runnable() {
            @Override
            public void run()  {
                try {

                    AnchorPane root = (AnchorPane)createView.getAnchorPane2() ;
                    Scene scene = new Scene(root);
                    Stage stage = new Stage();
                    stage.initModality(Modality.WINDOW_MODAL);
                    stage.initOwner(primaryStage);
                    stage.setScene(scene);
                    stage.showAndWait();
                }
                catch (IOException e){
                    e.printStackTrace();
                }
            }
        });

    }

}
