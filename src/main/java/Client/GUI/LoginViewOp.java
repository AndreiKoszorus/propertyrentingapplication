package Client.GUI;

import javafx.scene.control.*;

import java.awt.*;
import java.io.IOException;

/**
 * Created by Admin on 17.05.2018.
 */
public interface LoginViewOp {
    public PasswordField getPassword() ;

    public javafx.scene.control.TextField getTextField();

    public void showAdminWindow(CreateView createView) throws IOException;

    public void showUserWindow(CreateView createView) throws IOException;

    public void startApplication();

}
