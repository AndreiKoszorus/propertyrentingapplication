package Client.GUI;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by Admin on 19.05.2018.
 */
public class CreateView {
        private FXMLLoader adminLoader;
        private FXMLLoader loginLoader;
        private FXMLLoader regularUserLoader;
        private AdminView adminController;
        private RegularUserView regularUserWindowController;
        private FileInputStream fxmlStream;
        private FileInputStream fxmlStream2;
        private FileInputStream fxmlStream3;
        private AnchorPane anchorPane;
        private AnchorPane anchorPane2;
        private AnchorPane anchorPane3;
        public CreateView() throws IOException{

           this.adminLoader = new FXMLLoader();
           String fxmlPath="E:/Faculta/ASSIGMENT2/assigment2/src/main/java/Client/GUI/AdminWindow.fxml";
           this.fxmlStream=new FileInputStream(fxmlPath);
           this.anchorPane=(AnchorPane) adminLoader.load(fxmlStream);

           this.regularUserLoader=new FXMLLoader();
           String fxmlPath2="E:/Faculta/ASSIGMENT2/assigment2/src/main/java/Client/GUI/RegularUserWindow2.fxml";
           this.fxmlStream2=new FileInputStream(fxmlPath2);
           this.anchorPane2=(AnchorPane) regularUserLoader.load(fxmlStream2);
        }

    public AdminView getAdminView() {
        return (AdminView) adminLoader.getController();
    }

    public RegularUserView getRegularUserWindowView(){
        return  (RegularUserView) regularUserLoader.getController();
    }

    public AnchorPane getAdminLoader() throws IOException {
        return anchorPane;
    }

    public AnchorPane getAnchorPane2() throws IOException {
        return anchorPane2;
    }


}
