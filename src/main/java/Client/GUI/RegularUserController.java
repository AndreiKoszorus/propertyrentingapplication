package Client.GUI;

import Models.Ad;
import Models.FavouriteAds;
import Models.Message;
import Models.Ratings;
import Request.Request;

import java.io.IOException;
import java.util.List;

/**
 * Created by Admin on 21.05.2018.
 */
public class RegularUserController implements RegularUserControllerOP {

    private RegularUserViewOP regularUserViewOP;
    private String currentUser;
    public RegularUserController(RegularUserViewOP regularUserViewOP){
        this.regularUserViewOP=regularUserViewOP;
    }

    public void createAd() throws IOException{

            String location = regularUserViewOP.getCreateLocation().getText();
            String stringPrice = regularUserViewOP.getCreatePrice().getText();
            double price = Double.parseDouble(stringPrice);
            String available = (String) regularUserViewOP.getCreateAvailable().getValue();
            Ad ad = null;
            if (available.compareTo("Available") == 0) {
                ad = new Ad(currentUser,location, price, true);
            } else {
                ad = new Ad(currentUser,location, price, false);
            }
          Request request=new Request();
          request.setHead("USER-CREATE-AD");
          request.setBody(ad);
          LoginController.clientConnection.sendMessageToServer(request);


    }

    public void setCurrentUser(String currentUser){
        this.currentUser=currentUser;
    }

    public void notifyUser(){
        regularUserViewOP.setNotificationField("New message");
    }


    public void updateAd() throws IOException{


            String idString = regularUserViewOP.getIdToUpdate().getText();

            String location = regularUserViewOP.getUpdateLocation().getText();
            String priceString = regularUserViewOP.getUpdatePrice().getText();

            String idPlusLocation="";
            idPlusLocation+=idString+"-"+location+"-"+currentUser;

            double price = Double.parseDouble(priceString);
            String available = (String) regularUserViewOP.getUpdateAvailable().getValue();
            Ad ad = null;

            if (available.compareTo("Available") == 0) {
                ad = new Ad(currentUser, idPlusLocation, price, true);
            } else {
                ad = new Ad(currentUser,idPlusLocation, price, false);
            }

            Request request=new Request();
            request.setHead("USER-UPDATE-AD");
            request.setBody(ad);
            LoginController.clientConnection.sendMessageToServer(request);

    }

    public void deleteAd() throws IOException{
        String idString=regularUserViewOP.getAdNrDelete().getText();
        String idPlusUser="";
        idPlusUser+=idString+"-"+currentUser;
        Request request=new Request();
        request.setHead("USER-DELETE-AD");
        request.setBody(idPlusUser);
        LoginController.clientConnection.sendMessageToServer(request);
    }

    public void displayAds() throws IOException{
        String empty="";
        String location=regularUserViewOP.getDisplayLocation().getText().trim();
        String priceString=regularUserViewOP.getDisplayPrice().getText().trim();
        System.out.print(priceString);
        boolean isEmpty=regularUserViewOP.getFilterBy().getSelectionModel().isEmpty();

        String filter=(String)regularUserViewOP.getFilterBy().getValue();
        Request request=new Request();

        if(isEmpty){
            request.setHead("USER-DISPLAY-ADS");
        }
        else if(filter.compareTo("Price")==0 && priceString.compareTo(empty)!=0){
            request.setHead("USER-FILTER-PRICE");
            request.setBody(priceString);
        }
        else if(filter.compareTo("Location")==0 && location.compareTo(empty)!=0){
            request.setHead("USER-FILTER-LOCATION");
            request.setBody(location);
        }
        else if(filter.compareTo("Rating")==0 && location.compareTo(empty)!=0){
            String stars=(String) regularUserViewOP.getRateAdsFilter().getValue();
            String[] value=stars.split(" ");
            request.setHead("FILTER-BY-RATING");
            request.setBody(value[0]);
        }
        else{
            request.setHead("USER-DISPLAY-ADS");
        }
        LoginController.clientConnection.sendMessageToServer(request);
    }



    public void displayRequestedAds(List<Ad> ads){
        StringBuilder stringBuilder = new StringBuilder();
        for (Ad ad : ads) {
            stringBuilder.append("Ad id: ");
            stringBuilder.append(ad.getId());
            stringBuilder.append(" ");
            stringBuilder.append("Location: ");
            stringBuilder.append(ad.getLocation());
            stringBuilder.append(" ");
            stringBuilder.append("Owner: ");
            stringBuilder.append(ad.getOwner());
            stringBuilder.append(" ");
            stringBuilder.append("Price: ");
            stringBuilder.append(ad.getPrice());
            stringBuilder.append(" ");
            stringBuilder.append("Available: ");
            stringBuilder.append(ad.isAvailable());
            stringBuilder.append("\n");
        }
        String text = "" + stringBuilder;
        regularUserViewOP.setDisplayAds(text);
    }

    public void displayRequestedMessages(List<Message> messages){
        StringBuilder stringBuilder=new StringBuilder();
        for(Message message:messages){
            stringBuilder.append("From: ");
            stringBuilder.append(message.getFromUsername());
            stringBuilder.append(" ");
            stringBuilder.append("Message: ");
            stringBuilder.append(message.getText());
            stringBuilder.append("\n");
        }
        String text=""+stringBuilder;
        regularUserViewOP.setInboxField(text);
    }

    @Override
    public void displayMyAds() throws IOException {
        Request request=new Request();
        request.setHead("DISPLAY-MY-ADS");
        request.setBody(currentUser);
        LoginController.clientConnection.sendMessageToServer(request);
    }

    public void createFavouriteAd() throws IOException{

            String nr=regularUserViewOP.getFavouriteAdNr().getText();
            Long longNr=Long.parseLong(nr);
            FavouriteAds favouriteAds = new FavouriteAds(longNr,currentUser);
            Request request=new Request();
            request.setHead("ADD-FAVOURITE");
            request.setBody(favouriteAds);
            LoginController.clientConnection.sendMessageToServer(request);

    }

    public void displayFavouriteAds() throws IOException{
        Request request=new Request();
        request.setHead("DISPLAY-FAVOURITE");
        request.setBody(currentUser);
        LoginController.clientConnection.sendMessageToServer(request);
    }

    public void createMessage() throws IOException{
        String toUsername=regularUserViewOP.getChatUsername().getText();
        String message=regularUserViewOP.getMessageFields().getText();
        Message messageToSend=new Message(toUsername,currentUser,message);
        Request request=new Request();
        request.setHead("SEND-MESSAGE");
        request.setBody(messageToSend);
        LoginController.clientConnection.sendMessageToServer(request);

    }

    public void displayMessage() throws IOException{
        Request request=new Request();
        request.setHead("DISPLAY-MESSAGE");
        request.setBody(currentUser);
        regularUserViewOP.setNotificationField("");
        LoginController.clientConnection.sendMessageToServer(request);
    }

    public String getCurrentUser(){
        return currentUser;
    }

    public void deleteFavAds() throws IOException{
        Request request=new Request();
        Long id=Long.parseLong(regularUserViewOP.getRemoveFavouriteAds().getText());
        String idUsername="";
        idUsername+=id+"-"+currentUser;
        request.setHead("DELETE-FAV");
        request.setBody(idUsername);
        LoginController.clientConnection.sendMessageToServer(request);
    }

    public void createRating() throws IOException{
        Request request1=new Request();
        String stars=(String) regularUserViewOP.getRateAds().getValue();

        String[] value=stars.split(" ");
        Long id=Long.parseLong(regularUserViewOP.getRateId().getText());
        int rate=Integer.parseInt(value[0]);
        Ratings ratings=new Ratings(currentUser,id,rate);
        request1.setHead("CREATE-RATING");
        request1.setBody(ratings);
        LoginController.clientConnection.sendMessageToServer(request1);

    }


}
