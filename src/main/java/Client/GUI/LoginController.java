package Client.GUI;

import Client.Client;
import Request.Request;

import java.io.IOException;
import java.net.Socket;

/**
 * Created by Admin on 17.05.2018.
 */
public class LoginController implements LoginControllerOP {
  private LoginViewOp loginViewOp;
  private AdminViewOP adminControllerOP;
  public static Client.ClientConnection clientConnection;
  private CreateView createView;
  private String usernameToSend;

  public LoginController(LoginViewOp loginViewOp) throws IOException{
      this.loginViewOp=loginViewOp;
      this.createView=new CreateView();


  }


    public void openWindow() throws IOException{


         clientConnection=new Client.ClientConnection(new Socket("localhost",3000),this);
         clientConnection.start();

        usernameToSend=loginViewOp.getTextField().getText();
        String loginComp="";
        loginComp+=loginViewOp.getTextField().getText()+"-"+loginViewOp.getPassword().getText();




      Request request=new Request("LOGIN",loginComp);
      clientConnection.sendMessageToServer(request);
  }


    public void showAdmin() throws IOException{
        AdminController adminController=new AdminController(createView.getAdminView());
        createView.getAdminView().setAdminController(adminController);
        clientConnection.setAdminControllerOP(adminController);
        loginViewOp.showAdminWindow(createView);
  }
  public void showUser() throws IOException{
      RegularUserControllerOP regularUserControllerOP=new RegularUserController(createView.getRegularUserWindowView());
      regularUserControllerOP.setCurrentUser(usernameToSend);
      createView.getRegularUserWindowView().setRegulatUserControllerOP(regularUserControllerOP);
      clientConnection.setRegularUserControllerOP(regularUserControllerOP);
      loginViewOp.showUserWindow(createView);
  }

  public String getUsernameToSend(){
      return usernameToSend;
  }

  public void startApp(){
      loginViewOp.startApplication();
  }
}
