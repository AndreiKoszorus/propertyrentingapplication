package Client.GUI;

import Client.Client;
import Models.Ad;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.util.List;

/**
 * Created by Admin on 17.05.2018.
 */
public interface AdminViewOP {
    public TextArea getDisplayAds();
    public TextField getUsernameForAds() ;
    public void setDisplayAds(String text);
    public TextField getCreateLocation() ;

    public TextField getCreatePrice();

    public ComboBox getAvailableCombo() ;

    public TextField getUpdateAdNr() ;

    public TextField getUpdateLocation();

    public TextField getUpdatePrice();


    public ComboBox getUpdateAvailable();

    public TextField getDeleteAdNr();

    public TextField getUpdateName();

    public TextField getUpdateUsername();

    public TextField getUpdatePassword();

    public ComboBox getUpdateRoleCombo();

    public TextField getUpdateAccount();

    public TextField getDeleteUsername();

    public TextField getDisplayName();

    public TextField getDisplayUsername();

    public TextField getDisplayPassword();

    public TextField getDisplayRole();

    public TextField getDisplayAccount();

    public TextField getSearchedUsername();

    public TextField getUsernameToBeUpdated();

    public TextField getCreateOwner();

    public TextField getUpdateOwner();

    public ComboBox getTextType();

    public ComboBox getRateAdsUpdate();
    public TextField getRatingAdId();


    public TextField getGetUsernameToBeUpdated();

    public void setDisplayName(String displayName);

    public void setDisplayUsername(String displayUsername);

    public void setDisplayPassword(String displayPassword);

    public void setDisplayRole(String displayRole);

    public void setDisplayAccount(String displayAccount);
    public ComboBox getSubscriptionBox();

    public TextField getCreateSubUsername();
    public ComboBox getSubscriptionEditBox();
    public TextField getUsernameToEdit();



}
