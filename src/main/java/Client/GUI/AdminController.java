package Client.GUI;

import Client.GenerateReport.FileFactory;
import Models.Ad;
import Models.Subscription;
import Models.User;
import Request.Request;
import Client.GenerateReport.Files;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Admin on 20.05.2018.
 */
public class AdminController {
    private AdminViewOP adminViewOP;
    public AdminController(AdminViewOP adminViewOP){
        this.adminViewOP=adminViewOP;
    }
    public void displayAds() throws IOException{
        String username = adminViewOP.getUsernameForAds().getText();
        Request request=new Request();
        request.setHead("DISPLAY-ADS");
        request.setBody(username);
        LoginController.clientConnection.sendMessageToServer(request);
    }

    public void displayRequestedAds(List<Ad > ads){
        StringBuilder stringBuilder = new StringBuilder();
        System.out.print(ads.get(0).getOwner());
        for (Ad ad : ads) {
            stringBuilder.append("Ad id: ");
            stringBuilder.append(ad.getId());
            stringBuilder.append(" ");
            stringBuilder.append("Location: ");
            stringBuilder.append(ad.getLocation());
            stringBuilder.append(" ");
            stringBuilder.append("Owner: ");
            stringBuilder.append(ad.getOwner());
            stringBuilder.append(" ");
            stringBuilder.append("Price: ");
            stringBuilder.append(ad.getPrice());
            stringBuilder.append(" ");
            stringBuilder.append("Available: ");
            stringBuilder.append(ad.isAvailable());
            stringBuilder.append("\n");
        }
        String text = "" + stringBuilder;
        adminViewOP.setDisplayAds(text);
    }

    public void displayRequestedUsers(List<User> users){
        User user = users.get(0);
        adminViewOP.getDisplayName().setText(user.getName());
        adminViewOP.getDisplayUsername().setText(user.getUsername());
        adminViewOP.getDisplayPassword().setText(user.getPassword());
        if (user.isAdmin()) {
            adminViewOP.getDisplayRole().setText("Admin");
        } else {
            adminViewOP.getDisplayRole().setText("Regular User");
        }
        adminViewOP.getDisplayAccount().setText("" + user.getAccountNumber());
    }

    public void createUser() throws IOException{

            String username = adminViewOP.getUpdateUsername().getText();
            String name = adminViewOP.getUpdateName().getText();
            String password = adminViewOP.getUpdatePassword().getText();
            String role = (String) adminViewOP.getUpdateRoleCombo().getValue();
            int account = Integer.parseInt(adminViewOP.getUpdateAccount().getText());
            User user = null;

            if (role.compareTo("Regular User") == 0) {
                user = new User(name, username, password, false, account);
            } else {
                user = new User(name, username, password, false, account);
            }

            Request request=new Request();
            request.setHead("CREATE-USER");
            request.setBody(user);
            LoginController.clientConnection.sendMessageToServer(request);
    }

    public void updateUser() throws IOException{


            String username = adminViewOP.getUsernameToBeUpdated().getText();
            String name = adminViewOP.getUpdateName().getText();
            String password = adminViewOP.getUpdatePassword().getText();
            String role = (String) adminViewOP.getUpdateRoleCombo().getValue();

            int account = Integer.parseInt(adminViewOP.getUpdateAccount().getText());
            User user = null;

            if (role.compareTo("Regular User") == 0) {
                user = new User(name, username, password, false, account);
            } else {
                user = new User(name, username, password, true, account);
            }
            Request request=new Request();
            request.setHead("UPDATE-USER");
            request.setBody(user);
            LoginController.clientConnection.sendMessageToServer(request);
        }

    public void deleteUser() throws IOException{
        String username = adminViewOP.getDeleteUsername().getText();
        Request request=new Request();
        request.setHead("DELETE-USER");
        request.setBody(username);
        LoginController.clientConnection.sendMessageToServer(request);
    }

    public void displayUser() throws IOException{
        String searchedUsername=adminViewOP.getSearchedUsername().getText();
        Request request=new Request();
        request.setHead("DISPLAY-USER");
        request.setBody(searchedUsername);
        LoginController.clientConnection.sendMessageToServer(request);
    }

    public void createAd() throws IOException{
        Request request=new Request();
        request.setHead("CREATE-AD");
        String location = adminViewOP.getCreateLocation().getText();
        String stringPrice = adminViewOP.getCreatePrice().getText();
        double price = Double.parseDouble(stringPrice);
        String available = (String) adminViewOP.getAvailableCombo().getValue();
        String owner = adminViewOP.getCreateOwner().getText();
        Ad ad = null;
        if (available.compareTo("Available") == 0) {
            ad = new Ad(owner, location, price, true);
        } else {
            ad = new Ad(owner, location, price, false);
        }
        request.setBody(ad);
        LoginController.clientConnection.sendMessageToServer(request);
    }

    public void updateAd() throws IOException {

            String idString = adminViewOP.getUpdateAdNr().getText();
            String owner = adminViewOP.getUpdateOwner().getText();
            String ownerPlusId="";
            ownerPlusId+=idString+"-"+owner;
            String location = adminViewOP.getUpdateLocation().getText();
            String priceString = adminViewOP.getUpdatePrice().getText();
            double price = Double.parseDouble(priceString);
            String available = (String) adminViewOP.getUpdateAvailable().getValue();
            Ad ad = null;

            if (available.compareTo("Available") == 0) {
                ad = new Ad(ownerPlusId, location, price, true);
            } else {
                ad = new Ad(ownerPlusId, location, price, false);
            }
            Request request=new Request();
            request.setHead("UPDATE-AD");
            request.setBody(ad);
            LoginController.clientConnection.sendMessageToServer(request);
        }

        public void deleteAd() throws IOException{
                String idString = adminViewOP.getDeleteAdNr().getText();
                Long id = Long.parseLong(idString);
                Request request=new Request();
                request.setHead("DELETE-AD");
                request.setBody(id);
                LoginController.clientConnection.sendMessageToServer(request);

        }

        public void sendGenerate() throws IOException{
            Request request=new Request();
            request.setHead("GENERATE-FILE");
            LoginController.clientConnection.sendMessageToServer(request);
        }

        public void generate(List<Ad> ads) throws IOException{
            String type=(String) adminViewOP.getTextType().getValue();
            Frame parentFrame = new JFrame();

            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Specify a file to save");

            int userSelection = fileChooser.showSaveDialog(parentFrame);
            Files fileToGenerate= FileFactory.generateFiles(type);

            if (userSelection == JFileChooser.APPROVE_OPTION) {
                File fileToSave = fileChooser.getSelectedFile();
                if(fileToGenerate!=null) {
                    fileToGenerate.generateFile(fileToSave.getAbsolutePath(),ads);
                }
            }
        }

        public void editRaiting() throws IOException{
            Request request=new Request();
            String id=adminViewOP.getRatingAdId().getText();
            String stars=(String)adminViewOP.getRateAdsUpdate().getValue();
            String[] split=stars.split(" ");

            String idRate="";
            idRate+=id+"-"+split[0];

            request.setHead("EDIT-RAITING");
            request.setBody(idRate);
            LoginController.clientConnection.sendMessageToServer(request);

        }

        public void createSub() throws IOException{
            Request request=new Request();
            String username=adminViewOP.getCreateSubUsername().getText();
            String subType=(String)adminViewOP.getSubscriptionBox().getValue();
            Subscription subscription=null;
            if(subType.equals("Premium")){
                subscription=new Subscription(username,true,4, LocalDateTime.now());
            }
            else if(subType.equals("Regular")){
                subscription=new Subscription(username,false,4, LocalDateTime.now());
            }
            request.setHead("CREATE-SUB");
            request.setBody(subscription);
            LoginController.clientConnection.sendMessageToServer(request);

        }

        public void editSub() throws IOException{
            Request request=new Request();
            String username=adminViewOP.getUsernameToEdit().getText();
            String type=(String)adminViewOP.getSubscriptionEditBox().getValue();
            request.setHead("EDIT-SUB");
            String typeUsername="";
            if(type.equals("Premium")){
                typeUsername+="true"+"-"+username;
            }
            else if(type.equals("Regular")){
                typeUsername+="false"+"-"+username;
            }
            request.setBody(typeUsername);
            LoginController.clientConnection.sendMessageToServer(request);


        }


}
