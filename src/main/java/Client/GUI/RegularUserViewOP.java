package Client.GUI;

import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * Created by Admin on 21.05.2018.
 */
public interface RegularUserViewOP {
    public TextField getTestField();

    public TextField getCreateLocation();

    public TextField getCreatePrice();

    public ComboBox getCreateAvailable();

    public TextField getIdToUpdate();

    public TextField getUpdateLocation();

    public TextField getUpdatePrice();

    public ComboBox getUpdateAvailable();

    public TextField getAdNrDelete();

    public TextArea getDisplayAds();

    public ComboBox getFilterBy();

    public TextField getDisplayLocation();

    public TextField getDisplayPrice();

    public ComboBox getDisplayAvailable();

    public TextField getFavouriteAdNr();

    public TextArea getMessageField();

    public TextField getNotificationField();

    public TextField getRemoveFavouriteAds();

    public ComboBox getRateAds();
    public TextField getRateId() ;
    public ComboBox getRateAdsFilter();

    public void setDisplayAds(String displayAds);
    public void setInboxField(String message);

    public void setRegulatUserControllerOP(RegularUserControllerOP regulatUserControllerOP);
    public TextArea getMessageFields();

    public TextField getChatUsername();
    public void setNotificationField(String notificationField);

}
