package Client.GUI;

import Request.Request;
import Server.BL.ExceptionHendler;
import Models.Ad;
import Models.User;
import com.itextpdf.layout.element.Text;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Admin on 04.05.2018.
 */
public class AdminView implements AdminViewOP {
    @FXML
    private TextField createLocation;
    @FXML
    private TextField createPrice;
    @FXML
    private ComboBox availableCombo;

    @FXML
    private TextField updateAdNr;
    @FXML
    private TextField updateLocation;
    @FXML
    private TextField updatePrice;
    @FXML
    private ComboBox updateAvailable;

    @FXML
    private TextField deleteAdNr;
    @FXML
    private TextField updateName;
    @FXML
    private TextField updateUsername;
    @FXML
    private TextField updatePassword;
    @FXML
    private ComboBox updateRoleCombo;
    @FXML
    private TextField updateAccount;

    @FXML
    private TextField deleteUsername;
    @FXML
    private TextField displayName;
    @FXML
    private TextField displayUsername;
    @FXML
    private TextField displayPassword;
    @FXML
    private TextField displayRole;
    @FXML
    private TextField displayAccount;
    @FXML
    private TextField searchedUsername;
    @FXML
    private TextArea displayAds;
    @FXML
    private  TextField usernameForAds;
    @FXML
    private TextField usernameToBeUpdated;
    @FXML
    private TextField createOwner;
    @FXML
    private TextField updateOwner;
    @FXML
    private TextField ratingAdId;
    @FXML
    private ComboBox rateAdsUpdate;

    @FXML
    private ComboBox textType;
    @FXML
    private TextField getUsernameToBeUpdated;
    @FXML
    private TextField createSubUsername;
    @FXML
    private ComboBox subscriptionBox;
    @FXML
    private TextField usernameToEdit;
    @FXML
    private ComboBox subscriptionEditBox;

    private AdminController adminController;

    public TextField getCreateLocation() {
        return createLocation;
    }

    public TextField getCreatePrice() {
        return createPrice;
    }

    public ComboBox getAvailableCombo() {
        return availableCombo;
    }

    public TextField getUpdateAdNr() {
        return updateAdNr;
    }

    public TextField getUpdateLocation() {
        return updateLocation;
    }

    public TextField getUpdatePrice() {
        return updatePrice;
    }

    public ComboBox getUpdateAvailable() {
        return updateAvailable;
    }

    public TextField getDeleteAdNr() {
        return deleteAdNr;
    }

    public TextField getUpdateName() {
        return updateName;
    }

    public TextField getUpdateUsername() {
        return updateUsername;
    }

    public TextField getUpdatePassword() {
        return updatePassword;
    }

    public ComboBox getUpdateRoleCombo() {
        return updateRoleCombo;
    }

    public TextField getUpdateAccount() {
        return updateAccount;
    }

    public TextField getDeleteUsername() {
        return deleteUsername;
    }

    public TextField getDisplayName() {
        return displayName;
    }

    public TextField getDisplayUsername() {
        return displayUsername;
    }

    public TextField getDisplayPassword() {
        return displayPassword;
    }

    public TextField getDisplayRole() {
        return displayRole;
    }

    public TextField getDisplayAccount() {
        return displayAccount;
    }

    public TextField getSearchedUsername() {
        return searchedUsername;
    }

    public TextField getUsernameToBeUpdated() {
        return usernameToBeUpdated;
    }

    public TextField getCreateOwner() {
        return createOwner;
    }

    public TextField getUpdateOwner() {
        return updateOwner;
    }

    public ComboBox getTextType() {
        return textType;
    }

    public TextField getGetUsernameToBeUpdated() {
        return getUsernameToBeUpdated;
    }

    public ComboBox getRateAdsUpdate() {
        return rateAdsUpdate;
    }

    public TextField getRatingAdId() {
        return ratingAdId;
    }

    public void setDisplayName(String displayName) {
        this.displayName.setText(displayName);
    }

    public void setDisplayUsername(String displayUsername) {
        this.displayUsername.setText(displayUsername);
    }

    public void setDisplayPassword(String displayPassword) {
        this.displayPassword.setText(displayPassword);
    }

    public void setDisplayRole(String displayRole) {
        this.displayRole.setText(displayRole);
    }

    public void setDisplayAccount(String displayAccount) {
        this.displayAccount.setText(displayAccount);
    }

    public void setDisplayAds(String text) {
        this.displayAds.setText(text);
    }

    public TextField getUsernameForAds() {
        return usernameForAds;
    }

    public TextArea getDisplayAds() {
        return displayAds;
    }

    public ComboBox getSubscriptionBox() {
        return subscriptionBox;
    }

    public TextField getCreateSubUsername() {
        return createSubUsername;
    }

    public ComboBox getSubscriptionEditBox() {
        return subscriptionEditBox;
    }

    public TextField getUsernameToEdit() {
        return usernameToEdit;
    }

    public void setAdminController(AdminController adminController){
        this.adminController=adminController;
    }


    public void createUser() throws IOException {
        adminController.createUser();

    }

    public void updateUser() throws IOException {
        adminController.updateUser();
    }

    public void deleteUser() throws IOException {
        adminController.deleteUser();
    }

    public void displayUser() throws IOException {
        adminController.displayUser();

    }

    public void requestDisplayAds() throws IOException {
        adminController.displayAds();

    }


    public void createAd() throws IOException {
       adminController.createAd();
    }

    public void updateAd() throws IOException{
        adminController.updateAd();
    }

    public void deleteAd() throws IOException{
        adminController.deleteAd();
    }

    public void generate() throws IOException{
        adminController.sendGenerate();
    }

    public void editRate() throws IOException{
        adminController.editRaiting();
    }

    public void createSub() throws IOException{
        adminController.createSub();
    }
    public void editSub() throws IOException{
        adminController.editSub();
    }

}

