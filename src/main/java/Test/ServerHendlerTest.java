package Test;

import Models.Ad;
import Models.Ratings;
import Models.Subscription;
import Models.User;
import Request.Request;
import Server.BL.*;
import Server.DAO.UserDAO;
import Server.DAO.UserData;
import Server.IWriter;
import Server.ServerHendler;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import javax.jws.soap.SOAPBinding;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by Admin on 23.05.2018.
 */
@RunWith(MockitoJUnitRunner.class)
public class ServerHendlerTest {
    private ServerHendler serverHendler;
    private IWriter writer;
    private LoginVerification loginVerification;
    private AdminOp adminOp;
    private UserOp userOp;
    private FavouriteAdsOp favouriteAdsOp;
    private MessageOP messageOP;
    private UserData userData;
    private Validation validation;
    private RatingOP ratingOP;
    private SubOP subOP;
    @Before
    public void setUp() throws Exception {
        User user = new User("nume","nume123@gmail.com","numepass",true,123);
    }

    @Test
    public void hendleRequestDeleteUser() throws Exception {
        writer = mock(IWriter.class);
        userData=mock(UserData.class);
        adminOp=mock(AdminOp.class);
        userOp=mock(UserOp.class);
        favouriteAdsOp=mock(FavouriteAdsOp.class);
        messageOP=mock(MessageOP.class);
        loginVerification=mock(LoginVerification.class);
        ratingOP=mock(RatingOP.class);
        subOP=mock(SubOP.class);
        User user=new User("nume","nume123@gmail.com","numepass",true,123);
        List<User> userList=new ArrayList<>();
        userList.add(user);

        serverHendler = new ServerHendler(writer,loginVerification,adminOp,userOp,favouriteAdsOp,messageOP,ratingOP,subOP);
        Request request=new Request();
        request.setHead("DELETE-USER");
        request.setBody("nume123@gmail.com");

        final boolean[] connecta = {false};
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
               userList.remove(user);
                return null;
            }
        }).when(adminOp).deleteUser("nume123@gmail.com");
        serverHendler.hendleRequest(request);
        Assert.assertEquals(userList.size(),0);
    }
    @Test
    public void hendleRequestCreateAd() throws Exception {
        writer = mock(IWriter.class);
        userData=mock(UserData.class);
        adminOp=mock(AdminOp.class);
        userOp=mock(UserOp.class);
        favouriteAdsOp=mock(FavouriteAdsOp.class);
        messageOP=mock(MessageOP.class);
        loginVerification=mock(LoginVerification.class);
        ratingOP=mock(RatingOP.class);
        subOP=mock(SubOP.class);
        Ad ad = new Ad("george.farcas@gmail.com","Zorilor",234,true);
        List<Ad> adList=new ArrayList<>();


        serverHendler = new ServerHendler(writer,loginVerification,adminOp,userOp,favouriteAdsOp,messageOP,ratingOP,subOP);
        Request request=new Request();
        request.setHead("CREATE-AD");
        request.setBody(ad);

        final boolean[] connecta = {false};
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                adList.add(ad);
                return null;
            }
        }).when(adminOp).createAd(ad);
        serverHendler.hendleRequest(request);
        Assert.assertEquals(adList.size(),1);
    }
    @Test
    public void hendleRequestDeleteAd() throws Exception {
        writer = mock(IWriter.class);
        userData=mock(UserData.class);
        adminOp=mock(AdminOp.class);
        userOp=mock(UserOp.class);
        favouriteAdsOp=mock(FavouriteAdsOp.class);
        messageOP=mock(MessageOP.class);
        loginVerification=mock(LoginVerification.class);
        ratingOP=mock(RatingOP.class);
        subOP=mock(SubOP.class);
        Ad ad = new Ad("george.farcas@gmail.com","Zorilor",234,true);
        List<Ad> adList=new ArrayList<>();
        adList.add(ad);
        Long id=new Long(1);


        serverHendler = new ServerHendler(writer,loginVerification,adminOp,userOp,favouriteAdsOp,messageOP,ratingOP,subOP);
        Request request=new Request();
        request.setHead("DELETE-AD");
        request.setBody(id);

        final boolean[] connecta = {false};
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                adList.remove(ad);
                return null;
            }
        }).when(adminOp).deleteAd(id);
        serverHendler.hendleRequest(request);
        Assert.assertEquals(adList.size(),0);
    }
    @Test
    public void hendleRequestDisplayAds() throws Exception {
        writer = mock(IWriter.class);
        userData=mock(UserData.class);
        adminOp=mock(AdminOp.class);
        userOp=mock(UserOp.class);
        favouriteAdsOp=mock(FavouriteAdsOp.class);
        messageOP=mock(MessageOP.class);
        loginVerification=mock(LoginVerification.class);
        ratingOP=mock(RatingOP.class);
        subOP=mock(SubOP.class);
        Ad ad = new Ad("george.farcas@gmail.com","Zorilor",234,true);
        List<Ad> adList=new ArrayList<>();


        serverHendler = new ServerHendler(writer,loginVerification,adminOp,userOp,favouriteAdsOp,messageOP,ratingOP,subOP);
        Request request=new Request();
        Request request1=new Request();
        request.setHead("DISPLAY-ADS");
        request.setBody("george.farcas@gmail.com");
        request1.setHead("ADS-TO-DISPLAY");
        request1.setBody(adList);

        final boolean[] connecta = {false};
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                adList.add(ad);
                return null;
            }
        }).when(adminOp).selectAds("george.farcas@gmail.com");
        serverHendler.hendleRequest(request);
        Assert.assertEquals(adList.size(),1);
    }
    @Test
    public void createRating() throws Exception{
        writer = mock(IWriter.class);
        userData=mock(UserData.class);
        adminOp=mock(AdminOp.class);
        userOp=mock(UserOp.class);
        favouriteAdsOp=mock(FavouriteAdsOp.class);
        messageOP=mock(MessageOP.class);
        loginVerification=mock(LoginVerification.class);
        ratingOP=mock(RatingOP.class);
        subOP=mock(SubOP.class);



        serverHendler = new ServerHendler(writer,loginVerification,adminOp,userOp,favouriteAdsOp,messageOP,ratingOP,subOP);
        Request request=new Request();
        Long id=new Long(2);
        Ratings ratings=new Ratings("george.farcas@gmail.com",id,5);
        request.setHead("CREATE-RATING");
        request.setBody(ratings);
        serverHendler.hendleRequest(request);
        verify(ratingOP,times(1)).createRating(ratings);
    }
    @Test
    public void filterByRaiting() throws Exception{
        writer = mock(IWriter.class);
        userData=mock(UserData.class);
        adminOp=mock(AdminOp.class);
        userOp=mock(UserOp.class);
        favouriteAdsOp=mock(FavouriteAdsOp.class);
        messageOP=mock(MessageOP.class);
        loginVerification=mock(LoginVerification.class);
        ratingOP=mock(RatingOP.class);
        subOP=mock(SubOP.class);



        serverHendler = new ServerHendler(writer,loginVerification,adminOp,userOp,favouriteAdsOp,messageOP,ratingOP,subOP);
        Request request=new Request();
        request.setHead("FILTER-BY-RATING");
        String nr="5";
        request.setBody(nr);
        serverHendler.hendleRequest(request);
        verify(userOp,times(1)).searchByRate(5);

    }
    @Test
    public void createSub() throws Exception{
        writer = mock(IWriter.class);
        userData=mock(UserData.class);
        adminOp=mock(AdminOp.class);
        userOp=mock(UserOp.class);
        favouriteAdsOp=mock(FavouriteAdsOp.class);
        messageOP=mock(MessageOP.class);
        loginVerification=mock(LoginVerification.class);
        ratingOP=mock(RatingOP.class);
        subOP=mock(SubOP.class);



        serverHendler = new ServerHendler(writer,loginVerification,adminOp,userOp,favouriteAdsOp,messageOP,ratingOP,subOP);
        Subscription subscription=new Subscription("george.farcas@gmail.oom",true,4, LocalDateTime.now());
        Request request=new Request();
        request.setHead("CREATE-SUB");
        request.setBody(subscription);
        serverHendler.hendleRequest(request);
        verify(subOP,times(1)).createSub(subscription);

    }
    @Test
    public void displayFavourites() throws Exception{
        writer = mock(IWriter.class);
        userData=mock(UserData.class);
        adminOp=mock(AdminOp.class);
        userOp=mock(UserOp.class);
        favouriteAdsOp=mock(FavouriteAdsOp.class);
        messageOP=mock(MessageOP.class);
        loginVerification=mock(LoginVerification.class);
        ratingOP=mock(RatingOP.class);
        subOP=mock(SubOP.class);



        serverHendler = new ServerHendler(writer,loginVerification,adminOp,userOp,favouriteAdsOp,messageOP,ratingOP,subOP);
        Request request=new Request();
        request.setHead("DISPLAY-FAVOURITE");
        request.setBody("george.farcas@gmail.com");
        serverHendler.hendleRequest(request);
        verify(userOp,times(1)).getFavouriteAds("george.farcas@gmail.com");
    }

}