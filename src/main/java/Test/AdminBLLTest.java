package Test;

import Server.BL.*;
import Server.DAO.AdData;
import Server.DAO.UserData;
import Models.Ad;
import Models.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

/**
 * Created by Admin on 09.05.2018.
 */
@RunWith(MockitoJUnitRunner.class)
public class AdminBLLTest {
    private AdminBLL adminBLL;
    private List<User> users;
    private List<Ad> ads;
    private List<Ad>selectedAds;
    private List<User>selectedUsers;
    private UserOp userOp;
    private Validation validation;
    private UserData userData;
    private AdData adData;
    private FavouriteAdsOp favouriteAdsOp;
    private RatingOP ratingOP;
    @Before
    public void setUp() throws Exception {
        ads=new ArrayList<>();
        selectedAds=new ArrayList<>();
        users=new ArrayList<>();
        selectedUsers=new ArrayList<>();
        ads.add(new Ad("pop.vasile@yahoo.com","Marasti",250,true));
        ads.add(new Ad("farcas.george@gmail.com","Manastur",400,true));
        ads.add(new Ad("marius.ionescu@gmail.com","Meteor",350,true));
        selectedAds.add(new Ad("pop.vasile@yahoo.com","Marasti",250,true));
        selectedUsers.add(new User("Marius Ionescu","ionescu.marius@gmail.com","12345",false,11234));
        users.add(new User("Farcas George","farcas.george@gmail.com","12345",false,11234));
        users.add(new User("Pop Vasile","pop.vasile@yahoo.com","5555",false,11111));
        users.add(new User("Marius Ionescu","marius.ionescu@gmail.com","4444",false,242567));
    }

    @Test
    public void selectAds() throws Exception {
        adData=mock(AdData.class);
        validation=mock(Validation.class);
        userData=mock(UserData.class);
        favouriteAdsOp=mock(FavouriteAdsOp.class);
        ratingOP=mock(RatingOP.class);

        AdminBLL adminBLL=new AdminBLL(adData,userData,validation,favouriteAdsOp,ratingOP);
        when(adData.selectAds("pop.vasile@yahoo.com")).thenReturn(selectedAds);
        List<Ad>selectedAds1=adminBLL.selectAds("pop.vasile@yahoo.com");
        Assert.assertEquals(selectedAds.size(),selectedAds1.size());
    }


    @Test
    public void createAd() throws Exception{
        adData=mock(AdData.class);
        validation=mock(Validation.class);
        userData=mock(UserData.class);
        favouriteAdsOp=mock(FavouriteAdsOp.class);
        ratingOP=mock(RatingOP.class);
        List<Ad> ads=new ArrayList<>();
        Ad ad=new Ad("georgescu.marius@gmail.com","Marasti",250,true);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws ExceptionHendler {
                ads.add(ad);
                return null;
            }
        }).when(adData).createAd(ad);
        AdminBLL adminBLL=new AdminBLL(adData,userData,validation,favouriteAdsOp,ratingOP);
        adminBLL.createAd(ad);
        Assert.assertEquals(ads.size(),1);
    }

    @Test
    public void displayAllAds() throws Exception {
        adData=mock(AdData.class);
        validation=mock(Validation.class);
        userData=mock(UserData.class);
        favouriteAdsOp=mock(FavouriteAdsOp.class);
        ratingOP=mock(RatingOP.class);
        AdminBLL adminBLL=new AdminBLL(adData,userData,validation,favouriteAdsOp,ratingOP);
        when(adData.selectAllAds()).thenReturn(selectedAds);
        List<Ad>selectedAds1=adminBLL.displayAllAds();
        Assert.assertEquals(selectedAds.size(),selectedAds1.size());
    }

    @Test
    public void createUser() {
        adData=mock(AdData.class);
        validation=mock(Validation.class);
        userData=mock(UserData.class);
        favouriteAdsOp=mock(FavouriteAdsOp.class);
        ratingOP=mock(RatingOP.class);
        List<User> users=new ArrayList<>();
        User user=new User("Marius Ionescu","marius.ionescu@gmai;.com","12345",false,11234);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocationOnMock) throws Throwable {
                users.add(user);
                return null;
            }
        }).when(userData).createUser(user);
        AdminBLL adminBLL=new AdminBLL(adData,userData,validation,favouriteAdsOp,ratingOP);
        adminBLL.createUser(user);
        Assert.assertEquals(users.size(),1);

    }

    @Test
    public void selectUser() throws Exception {
        adData=mock(AdData.class);
        validation=mock(Validation.class);
        userData=mock(UserData.class);
        favouriteAdsOp=mock(FavouriteAdsOp.class);
        ratingOP=mock(RatingOP.class);
        AdminBLL adminBLL=new AdminBLL(adData,userData,validation,favouriteAdsOp,ratingOP);
        when(userData.selectUser("ionescu.marius@gmail.com")).thenReturn(selectedUsers);
        List<User> users1=adminBLL.selectUser("ionescu.marius@gmail.com");
        Assert.assertEquals(selectedUsers.size(),users1.size());


    }

}