package Server;

import Request.Request;

import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * Created by Admin on 23.05.2018.
 */
public class Writer implements IWriter {
    private ObjectOutputStream output;
    public Writer(ObjectOutputStream output){
        this.output=output;
    }
    @Override
    public void write(Request request) {
        try {
            output.writeObject(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
