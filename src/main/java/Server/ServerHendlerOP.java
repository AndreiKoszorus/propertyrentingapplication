package Server;

import Request.Request;

import java.io.IOException;

/**
 * Created by Admin on 22.05.2018.
 */
public interface ServerHendlerOP {
    public void hendleRequest(Request request) throws IOException;
    public Request getRequest();
}
