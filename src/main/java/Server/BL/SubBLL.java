package Server.BL;

import Models.Subscription;
import Server.DAO.SubData;

import java.time.LocalDateTime;

/**
 * Created by Admin on 31.05.2018.
 */
public class SubBLL implements SubOP {
    private SubData subData;
    private Validation validation;

    public SubBLL(SubData subData, Validation validation) {
        this.subData = subData;
        this.validation = validation;
    }

    public void createSub(Subscription subscription){
        try {
            validation.userExistance(subscription.getUsername());
            subData.createSub(subscription);
        }catch (ExceptionHendler e){
            System.out.print(e.getMessage());
        }
    }

    public void updateSubType(boolean type,String username){
        try {
            validation.userExistance(username);
            subData.updateSubType(type, username);
        }
        catch (ExceptionHendler e){
            System.out.print(e.getMessage());
        }
    }

    public void updateSubAds(String username,int ads){
        subData.updateSubAdNr(ads,username);
    }

    public void updateLastTime(LocalDateTime localDateTime,String username){
        subData.updateLastTimePosted(username,localDateTime);
    }

    public Subscription getSubscription(String username){
        return subData.getSubscription(username);
    }

}
