package Server.BL;

import Models.Subscription;

import java.time.LocalDateTime;

/**
 * Created by Admin on 31.05.2018.
 */
public interface SubOP {
    public void createSub(Subscription subscription);

    public void updateSubType(boolean type,String username);

    public void updateSubAds(String username,int ads);

    public void updateLastTime(LocalDateTime localDateTime, String username);

    public Subscription getSubscription(String username);

}
