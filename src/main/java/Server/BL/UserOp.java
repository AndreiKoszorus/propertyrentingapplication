package Server.BL;

import Models.Ad;
import Models.FavouriteAds;
import Models.Ratings;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Admin on 03.05.2018.
 */
public interface UserOp {
    public List<Ad> selectAds(String username) throws ExceptionHendler;

    public void createAd(Ad ad) throws ExceptionHendler;

    public void deleteAd(Long id,String username) throws ExceptionHendler;

    public void updateAd(Long id,Ad ad,String username)throws ExceptionHendler;

    public List<Ad> searchByLocation(String location)throws ExceptionHendler;

    public List<Ad> searchByPrice(String price)throws ExceptionHendler;

    public List<Ad> displayAllAds() throws ExceptionHendler;

    public List<Ad> getFavouriteAds(String username);

    public void createFavouriteAds(FavouriteAds favouriteAds);
    public List<Ad> searchByRate(int rate) throws ExceptionHendler;
}
