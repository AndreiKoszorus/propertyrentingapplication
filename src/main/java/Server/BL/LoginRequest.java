package Server.BL;

import Server.DAO.UserData;
import Models.User;

import java.util.List;

/**
 * Created by Admin on 02.05.2018.
 */
public class LoginRequest implements LoginVerification {
    private UserData userData;
    private Validation validation;

    public LoginRequest(UserData userData,Validation validation){
        this.userData=userData;
        this.validation=validation;
    }

    public boolean checkLogin(String username,String password){
        try {
            validation.userExistance(username);
            List<User> users=userData.selectUser(username);
            User user = users.get(0);
            String userPass = user.getPassword();
            validation.emailValidator(username);
            validation.userExistance(username);
            validation.passwordValidator(userPass, password);
        }
        catch (ExceptionHendler e){
            System.out.println(e.getMessage());
            return false;
        }
        return true;

    }


    public boolean checkRole(String username){
        List<User> users=userData.selectUser(username);
        User user=users.get(0);
        if(user.isAdmin()){
            return true;
        }
        return false;
    }


}
