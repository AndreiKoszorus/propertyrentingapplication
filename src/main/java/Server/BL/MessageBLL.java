package Server.BL;

import Models.Message;
import Server.DAO.MessageData;

import java.util.List;

/**
 * Created by Admin on 22.05.2018.
 */
public class MessageBLL implements MessageOP {
    private MessageData messageData;
    private Validation validation;
    public MessageBLL (MessageData messageData,Validation validation){
        this.messageData=messageData;
        this.validation=validation;
    }

    public void createMessage(Message message){
        try{
            validation.userExistance(message.getToUsername());
            messageData.createMessage(message);
        }
        catch (ExceptionHendler e){
            System.out.print(e.getMessage());
        }
    }

    public List<Message> displayMessages(String toUsername){
        List<Message> messages=null;
        try{
            validation.messagesExistance(toUsername);
            messages=messageData.displayMessage(toUsername);

        }
        catch (ExceptionHendler e){
            System.out.print(e.getMessage());
        }
        return messages;
    }
}
