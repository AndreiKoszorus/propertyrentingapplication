package Server.BL;


import Models.FavouriteAds;
import Models.Message;
import Models.Ratings;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Admin on 02.05.2018.
 */
public interface Validation {

    public void emailValidator(String username) throws ExceptionHendler;

    public boolean roleValidator(String username) throws ExceptionHendler;

    public void passwordValidator(String password,String insertedPass) throws ExceptionHendler;

    public void adExistance(Long id)throws ExceptionHendler;

    public void userExistance(String username) throws ExceptionHendler;

    public void validNumber(String price)throws ExceptionHendler;

    public void validId(String username,Long id) throws ExceptionHendler;

    public void availableAds(String username) throws ExceptionHendler;

    public void userAlreadyExist(String username) throws ExceptionHendler;

    public void favouriteAdExistance(String username) throws ExceptionHendler;
    public void messagesExistance(String toUsername) throws ExceptionHendler;
    public void checkFavAdExistance(Long id,String username) throws ExceptionHendler;
    public void alreadyExistsRating(String username,Long id)throws ExceptionHendler;
    public void ratingAdExistance(int rate) throws ExceptionHendler;
    public void checkRaiting(Long id) throws ExceptionHendler;
    public void checkPermissionToPost(boolean type, LocalDateTime localDateTime,int adNr) throws ExceptionHendler;
    }



