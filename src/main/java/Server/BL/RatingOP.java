package Server.BL;

import Models.Ratings;

import java.util.List;

/**
 * Created by Admin on 30.05.2018.
 */
public interface RatingOP {
    public void createRating(Ratings ratings) throws ExceptionHendler;
    public List<Ratings> findByRate(int rate) throws ExceptionHendler;
    public void deleteRaltedAds(Long id);
    public void editRaiting(Long id,int rate);
}
