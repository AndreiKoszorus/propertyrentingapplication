package Server.BL;

import Models.Ad;
import Models.User;

import java.util.List;

/**
 * Created by Admin on 03.05.2018.
 */
public interface AdminOp {
    public List<Ad> selectAds(String username) throws ExceptionHendler;

    public void createAd(Ad ad)throws ExceptionHendler;

    public void deleteAd(Long id) throws ExceptionHendler;

    public void updateAd(Long id,Ad ad) throws ExceptionHendler;

    public List<Ad> displayAllAds() throws ExceptionHendler;

    public void createUser(User user) throws ExceptionHendler;

    public List<User> selectUser(String username)throws ExceptionHendler;

    public void updateUser(User user) throws ExceptionHendler;

    public void deleteUser(String username)throws ExceptionHendler;

    public void generateFile(String type,String path) throws ExceptionHendler;
    public void editRaiting(Long id,int rate);
}
