package Server.BL;

import Models.Message;

import java.util.List;

/**
 * Created by Admin on 22.05.2018.
 */
public interface MessageOP {
    public void createMessage(Message message);

    public List<Message> displayMessages(String toUsername);
}
