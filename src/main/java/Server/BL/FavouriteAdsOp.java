package Server.BL;

import Models.Ad;
import Models.FavouriteAds;

import java.util.List;

/**
 * Created by Admin on 16.05.2018.
 */
public interface FavouriteAdsOp {

    public List<Ad> getFavouriteAds(String username);

    public void deleteRelatedAds(Long id);

    public void createFavouriteAd(FavouriteAds favouriteAds);
    public void deleteFavouriteAd(Long id,String username);
}
