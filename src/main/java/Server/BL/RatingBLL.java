package Server.BL;

import Models.Ratings;
import Server.DAO.RatingData;

import java.util.List;

/**
 * Created by Admin on 30.05.2018.
 */
public class RatingBLL implements RatingOP {
    private RatingData ratingData;
    private Validation validation;

    public RatingBLL(RatingData ratingData, Validation validation) {
        this.ratingData = ratingData;
        this.validation = validation;
    }

    public void createRating(Ratings ratings){
        try{
            validation.alreadyExistsRating(ratings.getUsername(),ratings.getAdId());
            ratingData.createRating(ratings);
        }catch (ExceptionHendler e){
            System.out.print(e.getMessage());
        }
    }

    public List<Ratings> findByRate(int rate) throws ExceptionHendler{
        List<Ratings>ratingsList=null;

            validation.ratingAdExistance(rate);
            ratingsList=ratingData.findAdByRate(rate);


        return ratingsList;
    }

    public void deleteRaltedAds(Long id){
        ratingData.deleteRelatedAds(id);
    }

    public void editRaiting(Long id,int rate){
        try {
            validation.checkRaiting(id);
            ratingData.updateRaiting(rate,id);
        }
        catch (ExceptionHendler e){
            System.out.print(e.getMessage());
        }
    }
}
