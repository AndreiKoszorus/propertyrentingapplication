package Server.BL;


import Server.DAO.AdData;
import Server.DAO.FavouriteAdData;
import Models.Ad;
import Models.FavouriteAds;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 15.05.2018.
 */
public class FavouriteAdBLL implements FavouriteAdsOp {
    private FavouriteAdData favouriteAdData;
    private AdData adData;
    private Validation validation;

    public FavouriteAdBLL(FavouriteAdData favouriteAdData, AdData adData, Validation validation) {
        this.favouriteAdData = favouriteAdData;
        this.adData = adData;
        this.validation = validation;
    }

    public List<Ad> getFavouriteAds(String username) {
        List<Ad> favAds = new ArrayList<>();
        try {
            validation.favouriteAdExistance(username);
            List<FavouriteAds> favouriteAds = favouriteAdData.selectFavouriteAds(username);
            List<Ad> ads;

            for (FavouriteAds ad : favouriteAds) {
                ads = adData.findAdById(ad.getAdId());
                favAds.add(ads.get(0));
            }
        } catch (ExceptionHendler e) {
            System.out.print(e.getMessage());
        }

        return favAds;
    }

    public void deleteRelatedAds(Long id){
        favouriteAdData.deleteRelatedAds(id);
    }

    public void createFavouriteAd(FavouriteAds favouriteAds){
        try {
            validation.adExistance(favouriteAds.getAdId());
            favouriteAdData.createFavouriteAd(favouriteAds);
        }
        catch (ExceptionHendler e){
            System.out.print(e.getMessage());
        }
    }

    public void deleteFavouriteAd(Long id,String username){
        try {
            validation.checkFavAdExistance(id,username);
            favouriteAdData.deleteFavouriteAd(id,username);
        } catch (ExceptionHendler exceptionHendler) {
            System.out.print(exceptionHendler.getMessage());
        }

    }
}
