package Server.BL;

import Client.GenerateReport.FileFactory;
import Client.GenerateReport.Files;
import Server.DAO.AdData;
import Server.DAO.UserData;
import Models.Ad;
import Models.User;

import java.util.List;


/**
 * Created by Admin on 03.05.2018.
 */
public class AdminBLL implements AdminOp {

    private AdData adData;
    private UserData userData;
    private Validation validation;
    private FavouriteAdsOp favouriteAdsOp;
    private RatingOP ratingOP;

    public AdminBLL(AdData adData,UserData userData,Validation validation,FavouriteAdsOp favouriteAdsOp,RatingOP ratingOP){
        this.adData=adData;
        this.userData=userData;
        this.validation=validation;
        this.favouriteAdsOp=favouriteAdsOp;
        this.ratingOP=ratingOP;
    }

    public List<Ad> selectAds(String username) {

        try {
            validation.availableAds(username);

        }
        catch (ExceptionHendler e) {
            System.out.println(e.getMessage());
        }
        return adData.selectAds(username);
    }

    public void createAd(Ad ad) {
        try {
            validation.emailValidator(ad.getOwner());
            validation.userExistance(ad.getOwner());

            adData.createAd(ad);
        }
        catch(ExceptionHendler e){
            System.out.print(e.getMessage());
        }
    }

    public void deleteAd(Long id) {
        try {
            validation.adExistance(id);

            adData.deleteAd(id);
            favouriteAdsOp.deleteRelatedAds(id);
            ratingOP.deleteRaltedAds(id);
        } catch (ExceptionHendler e) {
            System.out.println(e.getMessage());
        }

    }

    public void updateAd(Long id,Ad ad){
        try {

            validation.adExistance(id);

            adData.updateAd(id,ad);
        } catch (ExceptionHendler e) {
            System.out.println(e.getMessage());
        }
    }
    public List<Ad> displayAllAds(){
        return adData.selectAllAds();
    }

    public void createUser(User user){
        try {
            validation.userAlreadyExist(user.getUsername());
            validation.emailValidator(user.getUsername());
            userData.createUser(user);
        }
        catch (ExceptionHendler e){
            System.out.print(e.getMessage());
        }
    }

    public List<User> selectUser(String username){
        List<User> users=null;
        try {
            validation.userExistance(username);
            users = userData.selectUser(username);
        }
        catch (ExceptionHendler e){
            System.out.println(e.getMessage());
        }

        return users;
    }

    public void updateUser(User user){
        try{
            validation.userExistance(user.getUsername());
            userData.updateUser(user);
        }
        catch (ExceptionHendler e){
            System.out.println(e.getMessage());
        }
    }

    public void deleteUser(String username){
        try{
            validation.userExistance(username);
            userData.deleteUser(username);
        }
        catch (ExceptionHendler e){
            System.out.println(e.getMessage());
        }
    }

    public void generateFile(String type,String path){
        List<Ad> ads=adData.selectAllAds();
        Files fileToGenerate= FileFactory.generateFiles(type);
        if(fileToGenerate!=null) {
            fileToGenerate.generateFile(path,ads);
        }

    }
    public void editRaiting(Long id,int rate){
        ratingOP.editRaiting(id,rate);
    }

}
