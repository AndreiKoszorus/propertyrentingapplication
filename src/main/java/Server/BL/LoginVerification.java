package Server.BL;

/**
 * Created by Admin on 02.05.2018.
 */
public interface LoginVerification {

    public boolean checkLogin(String username,String password);
    public boolean checkRole(String username);
}
