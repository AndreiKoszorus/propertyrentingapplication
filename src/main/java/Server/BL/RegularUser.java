package Server.BL;

import Models.Ratings;
import Models.Subscription;
import Server.DAO.AdData;
import Models.Ad;
import Models.FavouriteAds;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by Admin on 02.05.2018.
 */
public class RegularUser implements UserOp {
    private AdData adData;
    private Validation validation;
    private FavouriteAdsOp favouriteAdsOp;
    private RatingOP ratingOP;
    private SubOP subOP;

    public RegularUser(AdData adData, Validation validation, FavouriteAdsOp favouriteAdsOp,RatingOP ratingOP,SubOP subOP) {
        this.adData = adData;
        this.validation = validation;
        this.favouriteAdsOp=favouriteAdsOp;
        this.ratingOP=ratingOP;
        this.subOP=subOP;
    }

    public List<Ad> selectAds(String username) {
        try {
            validation.availableAds(username);

        }
        catch (ExceptionHendler e) {
            System.out.println(e.getMessage());
        }
        return adData.selectAds(username);
    }

    public void createAd(Ad ad) {
         try {

             Subscription subscription=subOP.getSubscription(ad.getOwner());
             validation.checkPermissionToPost(subscription.isType(),subscription.getLastTimePosted(),subscription.getLimitedAds());
             if(subscription.getLimitedAds()==0 || subscription.isType()){
                 subOP.updateSubAds(ad.getOwner(),3);
             }
             else {
                 subOP.updateSubAds(ad.getOwner(), subscription.getLimitedAds() - 1);
             }
             subOP.updateLastTime(LocalDateTime.now(),ad.getOwner());
             adData.createAd(ad);
         }
         catch (ExceptionHendler e){
             System.out.print(e.getMessage());
         }
    }

    public void deleteAd(Long id,String username) {
        try {
            validation.adExistance(id);
            validation.validId(username, id);

            adData.deleteAd(id);
            favouriteAdsOp.deleteRelatedAds(id);
            ratingOP.deleteRaltedAds(id);
        } catch (ExceptionHendler e) {
            System.out.println(e.getMessage());
        }

    }

    public void updateAd(Long id,Ad ad,String username){
        try {

            validation.adExistance(id);

            validation.validId(username, id);

            adData.updateAd(id,ad);
        } catch (ExceptionHendler e) {
            System.out.println(e.getMessage());
        }
    }

    public List<Ad> searchByLocation(String location){
       return adData.findAdByLocation(location);
    }

    public List<Ad> searchByPrice(String price){
        double realPrice=0;
        try {
            validation.validNumber(price);
            realPrice=Double.parseDouble(price);
        }
        catch (ExceptionHendler e){
            System.out.println(e.getMessage());
        }
        return adData.findAdByPrice(realPrice);
    }

    public List<Ad> displayAllAds(){
        return adData.selectAllAds();
    }

    public List<Ad> getFavouriteAds(String username){

        return favouriteAdsOp.getFavouriteAds(username);
    }

    public void createFavouriteAds(FavouriteAds favouriteAds){
        favouriteAdsOp.createFavouriteAd(favouriteAds);
    }

    public List<Ad> searchByRate(int rate) throws ExceptionHendler{
        List<Ad> ads = new ArrayList<>();

            List<Ratings> ratingsList = ratingOP.findByRate(rate);
            HashSet<Long> adId = new HashSet<>();

            if (ratingsList.size() != 0) {
                for (Ratings ratings : ratingsList) {
                    adId.add(ratings.getAdId());
                }

            }
            if (adId.size() > 0) {
                for (Long l : adId) {
                    Ad ad = adData.findAdById(l).get(0);
                    ads.add(ad);
                }
            }


        return ads;
    }
}
