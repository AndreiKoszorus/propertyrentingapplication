package Server;


import Models.Message;
import Request.Request;
import Server.BL.*;
import Server.DAO.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.Instant;
import java.util.ArrayList;

/**
 * Created by Admin on 16.05.2018.
 */
public  class Server extends Thread  {
    public static ArrayList<ServerHendlerOP> connections=new ArrayList<>();
    public static class ServerConnection  extends Thread{
        private final Socket clientSocket;
        private ServerHendlerOP serverHendlerOP;
        private ObjectOutputStream output;


        public ServerConnection (Socket clientSocket,ServerHendlerOP serverHendlerOP) throws IOException{
            this.clientSocket=clientSocket;
            this.serverHendlerOP=serverHendlerOP;


        }

        public void run(){
            try(
                ObjectInputStream input = new ObjectInputStream(clientSocket.getInputStream())) {


                while (clientSocket.isConnected()) {
                    boolean clientHasData = clientSocket.getInputStream().available() > 0;
                    if (clientHasData) {
                        Request request = (Request) input.readObject();
                        serverHendlerOP.hendleRequest(request);


                    }

                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
                System.out.print(Instant.now()+"Client disconnected...");
            }
            catch(IOException | ClassNotFoundException e){
                e.printStackTrace();
            }
            try{
                clientSocket.close();
            }
            catch (IOException e){
                e.printStackTrace();
            }
        }

        public ObjectOutputStream getOutput() {
            return output;
        }
    }
    public static void main(String[] args) throws IOException, ClassNotFoundException
    {
        try (ServerSocket socket = new ServerSocket(3000))
        {
            while (true)
            {
                System.out.println(Instant.now() + " Waiting for client...");
                Socket clientSocket = socket.accept();
                UserDAO userDAO=new UserDAO();
                AdDAO adDAO=new AdDAO();
                FavouriteAdDAO favouriteAdDAO=new FavouriteAdDAO();
                MessageDAO messageDAO=new MessageDAO();
                RatingDAO ratingDAO=new RatingDAO();
                SubDAO subDAO=new SubDAO();

                Validators validators=new Validators(adDAO,userDAO,favouriteAdDAO,messageDAO,ratingDAO);
                FavouriteAdBLL favouriteAdBLL=new FavouriteAdBLL(favouriteAdDAO,adDAO,validators);

                RatingBLL ratingBLL=new RatingBLL(ratingDAO,validators);
                AdminBLL adminBLL=new AdminBLL(adDAO,userDAO,validators,favouriteAdBLL,ratingBLL);
                SubBLL subBLL=new SubBLL(subDAO,validators);
                RegularUser regularUser=new RegularUser(adDAO,validators,favouriteAdBLL,ratingBLL,subBLL);
                MessageBLL messageBLL=new MessageBLL(messageDAO,validators);


                LoginRequest loginRequest=new LoginRequest(userDAO,validators);
                ServerHendler serverHendler=new ServerHendler(new Writer(new ObjectOutputStream(clientSocket.getOutputStream())),loginRequest,adminBLL,regularUser,favouriteAdBLL,messageBLL,ratingBLL,subBLL);
                connections.add(serverHendler);
                new ServerConnection(clientSocket,serverHendler).start();
            }
        }
    }
}
