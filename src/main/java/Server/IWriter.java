package Server;

import Request.Request;

/**
 * Created by Admin on 23.05.2018.
 */
public interface IWriter {
    public void write(Request request);
}
