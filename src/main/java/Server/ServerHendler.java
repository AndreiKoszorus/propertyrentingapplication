package Server;


import Models.*;
import Server.BL.*;
import Request.Request;

import java.io.IOException;
import java.util.List;

/**
 * Created by Admin on 22.05.2018.
 */

public class ServerHendler implements ServerHendlerOP {
    private Request sendBack;
    private IWriter writer;
    private LoginVerification loginVerification;
    private AdminOp adminOp;
    private UserOp userOp;
    private FavouriteAdsOp favouriteAdsOp;
    private MessageOP messageOP;
    private RatingOP ratingOP;
    private SubOP subOP;
    public ServerHendler(IWriter writer, LoginVerification loginVerification, AdminOp adminOp, UserOp userOp,FavouriteAdsOp favouriteAdsOp,MessageOP messageOP,RatingOP ratingOP,SubOP subOP) throws IOException{

        this.writer=writer;
        this.loginVerification=loginVerification;
        this.adminOp=adminOp;
        this.userOp=userOp;
        this.favouriteAdsOp=favouriteAdsOp;
        this.messageOP=messageOP;
        this.ratingOP=ratingOP;
        this.subOP=subOP;
    }
    public void hendleRequest(Request request) throws IOException{
        sendBack=new Request();
        switch(request.getHead()){
            case "LOGIN":
                String comp=(String)request.getBody();
                String [] splited=comp.split("-");
                String username=splited[0];
                String password=splited[1];
                if(loginVerification.checkLogin(username,password)){
                    if(loginVerification.checkRole(username)){
                        sendBack.setHead("ADMIN");
                    }
                    else{
                        sendBack.setHead("USER");
                    }
                    writer.write(sendBack);
                }
                break;
            case "DISPLAY-ADS":
                List<Ad> ads=null;
                try {
                    String usernameToDisplay = (String) request.getBody();
                    ads = adminOp.selectAds(usernameToDisplay);


                }
                catch (ExceptionHendler e){
                    System.out.print(e.getMessage());
                }
                sendBack.setHead("ADS-TO-DISPLAY");
                sendBack.setBody(ads);
                writer.write(sendBack);
                break;
            case "CREATE-USER":
                User user=(User)request.getBody();
                try {
                    adminOp.createUser(user);
                } catch (ExceptionHendler exceptionHendler) {
                    exceptionHendler.printStackTrace();
                }
                sendBack.setHead("USER-CREATED");
                writer.write(sendBack);
                break;

            case "UPDATE-USER":
                User userUpdate=(User) request.getBody();
                try {
                    adminOp.updateUser(userUpdate);
                } catch (ExceptionHendler exceptionHendler) {
                    exceptionHendler.printStackTrace();
                }
                break;

            case "DELETE-USER":
                String deleteUsername=(String)request.getBody();
                try {
                    adminOp.deleteUser(deleteUsername);
                } catch (ExceptionHendler exceptionHendler) {
                    exceptionHendler.printStackTrace();
                }
                break;
            case "DISPLAY-USER":
                String displayedUser=(String)request.getBody();
                List<User> users= null;
                try {
                    users = adminOp.selectUser(displayedUser);
                } catch (ExceptionHendler exceptionHendler) {
                    exceptionHendler.printStackTrace();
                }
                sendBack.setHead("DISPLAY-THE-USERS");
                sendBack.setBody(users);
                writer.write(sendBack);
                break;
            case "CREATE-AD":
                Ad ad=(Ad) request.getBody();
                try {
                    adminOp.createAd(ad);
                } catch (ExceptionHendler exceptionHendler) {
                    exceptionHendler.printStackTrace();
                }
                break;

            case "UPDATE-AD":
                Ad updateAd=(Ad) request.getBody();
                String [] split=updateAd.getOwner().split("-");
                Long id=Long.parseLong(split[0]);
                updateAd.setOwner(split[1]);
                try {
                    adminOp.updateAd(id,updateAd);
                } catch (ExceptionHendler exceptionHendler) {
                    exceptionHendler.printStackTrace();
                }
                break;

            case "DELETE-AD":
                Long deletedId=(Long) request.getBody();
                try {
                    adminOp.deleteAd(deletedId);
                    favouriteAdsOp.deleteRelatedAds(deletedId);
                } catch (ExceptionHendler exceptionHendler) {
                    exceptionHendler.printStackTrace();
                }
                break;
            case "GENERATE-FILE":
                List<Ad> allAds= null;
                try {
                    allAds = adminOp.displayAllAds();
                } catch (ExceptionHendler exceptionHendler) {
                    exceptionHendler.printStackTrace();
                }
                sendBack.setHead("ADS-FOR-FILE");
                sendBack.setBody(allAds);
                writer.write(sendBack);
                break;
            case "USER-CREATE-AD":
                Ad userCreateAd=(Ad)request.getBody();
                try {
                    userOp.createAd(userCreateAd);
                } catch (ExceptionHendler exceptionHendler) {
                    exceptionHendler.printStackTrace();
                }

                break;
            case "USER-UPDATE-AD":
                Ad userUpdateAd=(Ad)request.getBody();
                String[] split2=userUpdateAd.getLocation().split("-");
                Long idUserUpdate=Long.parseLong(split2[0]);
                userUpdateAd.setLocation(split2[1]);
                //System.out.print(userUpdateAd.getOwner());
                try {
                    userOp.updateAd(idUserUpdate,userUpdateAd, userUpdateAd.getOwner());
                } catch (ExceptionHendler exceptionHendler) {
                    exceptionHendler.printStackTrace();
                }
                break;
            case "USER-DELETE-AD":
                String idPlusUser=(String)request.getBody();
                String[] split3=idPlusUser.split("-");
                Long idUserDelete=Long.parseLong(split3[0]);
                try {
                    userOp.deleteAd(idUserDelete,split3[1]);
                    favouriteAdsOp.deleteRelatedAds(idUserDelete);

                } catch (ExceptionHendler exceptionHendler) {
                    exceptionHendler.printStackTrace();
                }
                break;
            case "USER-DISPLAY-ADS":
                List<Ad> displayAds= null;
                try {
                    displayAds = userOp.displayAllAds();
                } catch (ExceptionHendler exceptionHendler) {
                    exceptionHendler.printStackTrace();
                }
                sendBack.setHead("DISPLAY-ALL-ADS");
                sendBack.setBody(displayAds);
                writer.write(sendBack);
                break;
            case "USER-FILTER-LOCATION":
                String location=(String)request.getBody();
                List<Ad> filterLocation= null;
                try {
                    filterLocation = userOp.searchByLocation(location);
                } catch (ExceptionHendler exceptionHendler) {
                    exceptionHendler.printStackTrace();
                }
                sendBack.setHead("DISPLAY-LOCATION-FILTER");
                sendBack.setBody(filterLocation);
                writer.write(sendBack);
                break;
            case "USER-FILTER-PRICE":
                String priceString=(String)request.getBody();
                List<Ad> filterPrice= null;
                try {
                    filterPrice = userOp.searchByPrice(priceString);
                } catch (ExceptionHendler exceptionHendler) {
                    exceptionHendler.printStackTrace();
                }

                sendBack.setHead("DISPLAY-PRICE-FILTER");
                sendBack.setBody(filterPrice);
                writer.write(sendBack);
                break;
            case "DISPLAY-MY-ADS":
                String currentUser=(String) request.getBody();
                List<Ad> myAds = null;
                try {
                    myAds = userOp.selectAds(currentUser);
                } catch (ExceptionHendler exceptionHendler) {
                    exceptionHendler.printStackTrace();
                }
                sendBack.setHead("MY-ADS");
                sendBack.setBody(myAds);
                writer.write(sendBack);
                break;
            case "ADD-FAVOURITE":
                FavouriteAds favouriteAds=(FavouriteAds)request.getBody();
                favouriteAdsOp.createFavouriteAd(favouriteAds);
                break;
            case "DISPLAY-FAVOURITE":
                String usernameToDisplayFav=(String) request.getBody();
                List<Ad> favAds= userOp.getFavouriteAds(usernameToDisplayFav);
                sendBack.setHead("DISPLAY-FAV");
                sendBack.setBody(favAds);
                writer.write(sendBack);
                break;
            case "SEND-MESSAGE":
                Message message=(Message) request.getBody();
                messageOP.createMessage(message);
                for(ServerHendlerOP serverHendler: Server.connections){
                    Request request1=new Request();

                    request1.setHead("NOTIFY-USERS");
                    request1.setBody(message.getToUsername());
                    serverHendler.hendleRequest(request1);
                }
                break;
            case "DISPLAY-MESSAGE":
                String toUsername=(String) request.getBody();
                List<Message> messages=messageOP.displayMessages(toUsername);
                System.out.print(toUsername);
                for(Message message1:messages){
                    System.out.print(message1.getFromUsername());
                }
                sendBack.setHead("DISPLAY-REQUESTED-ADS");
                sendBack.setBody(messages);
                writer.write(sendBack);
                break;
            case "NOTIFY-USERS":
                sendBack.setHead("NOTIFY");
                sendBack.setBody(request.getBody());
                writer.write(sendBack);
                break;
            case "DELETE-FAV":
                String idUsername=(String) request.getBody();
                String [] split1=idUsername.split("-");
                Long id1=Long.parseLong(split1[0]);
                favouriteAdsOp.deleteFavouriteAd(id1,split1[1]);
                break;
            case "CREATE-RATING":
                try {
                    Ratings ratings = (Ratings) request.getBody();
                    ratingOP.createRating(ratings);
                }catch (ExceptionHendler e){
                    System.out.print(e.getMessage());
                }
                break;
            case "FILTER-BY-RATING":
                try {
                    String rate = (String) request.getBody();
                    List<Ad> ads1 = userOp.searchByRate(Integer.parseInt(rate));
                    sendBack.setHead("DISPLAY-RATING-FILTER");
                    sendBack.setBody(ads1);
                }
                catch (ExceptionHendler e){
                    System.out.print(e.getMessage());
                }
                break;
            case "EDIT-RAITING":
                String idRate=(String) request.getBody();
                String[] s2=idRate.split("-");
                adminOp.editRaiting(Long.parseLong(s2[0]),Integer.parseInt(s2[1]));
                break;
            case "CREATE-SUB":
                Subscription subscription=(Subscription)request.getBody();
                subOP.createSub(subscription);
                break;
            case "EDIT-SUB":
                String typeUsername=(String)request.getBody();
                String[] s=typeUsername.split("-");
                if(s[0].equals("true")){
                    subOP.updateSubType(true,s[1]);
                }
                else{
                    subOP.updateSubType(false,s[1]);
                }
                break;





        }



    }

    @Override
    public Request getRequest() {
        return sendBack;

    }

}
