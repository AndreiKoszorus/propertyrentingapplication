package Server.DAO;

import Models.Subscription;
import Models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 31.05.2018.
 */
public class SubDAO implements SubData {
    private final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
    private SessionFactory sessionFactory;

    public SubDAO() {
        try {
            this.sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        } catch (Exception e) {
            StandardServiceRegistryBuilder.destroy(registry);
            System.err.println("Error in hibernate: ");
            e.printStackTrace();

        }
    }

    public void createSub(Subscription subscription) {
        try(Session session=sessionFactory.openSession()){
            session.beginTransaction();
            session.save(subscription);
            session.getTransaction().commit();
        }
    }

    public void updateSubAdNr(int nr,String username){
        try(Session session=sessionFactory.openSession()) {
            session.beginTransaction();
            javax.persistence.Query query = session.createQuery("from Subscription where username = :user");
            query.setParameter("user", username);
            List<Subscription> subscriptions = query.getResultList();
            Subscription updateSub = subscriptions.get(0);
            updateSub.setLimitedAds(nr);
            session.update(updateSub);
            session.getTransaction().commit();
        }
    }

    public void updateSubType(boolean type,String username){
        try(Session session=sessionFactory.openSession()) {
            session.beginTransaction();
            javax.persistence.Query query = session.createQuery("from Subscription where username = :user");
            query.setParameter("user", username);
            List<Subscription> subscriptions = query.getResultList();
            Subscription updateSub = subscriptions.get(0);
            updateSub.setType(type);
            session.update(updateSub);
            session.getTransaction().commit();
        }
    }

    public void updateLastTimePosted(String username,LocalDateTime date){
        try(Session session=sessionFactory.openSession()) {
            session.beginTransaction();
            javax.persistence.Query query = session.createQuery("from Subscription where username = :user");
            query.setParameter("user", username);
            List<Subscription> subscriptions = query.getResultList();
            Subscription updateSub = subscriptions.get(0);
            updateSub.setLastTimePosted(date);
            session.update(updateSub);
            session.getTransaction().commit();
        }

    }

    public Subscription getSubscription(String username){
            List<Subscription> subscriptions=null;
        try(Session session=sessionFactory.openSession()) {
            session.beginTransaction();
            javax.persistence.Query query = session.createQuery("from Subscription where username = :user");
            query.setParameter("user", username);
            subscriptions = query.getResultList();
        }
        return subscriptions.get(0);
    }
}
