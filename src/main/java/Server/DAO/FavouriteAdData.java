package Server.DAO;

import Models.FavouriteAds;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by Admin on 15.05.2018.
 */
public interface FavouriteAdData {
    public void createFavouriteAd(FavouriteAds favouriteAds);
    public List<FavouriteAds> selectFavouriteAds(String username);

    public void deleteFavouriteAd(Long id,String username);

    public void deleteRelatedAds(Long id);

    public List<FavouriteAds> findById(Long id,String username);
}
