package Server.DAO;

import Models.Message;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by Admin on 22.05.2018.
 */
public class MessageDAO implements MessageData {

    private final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
    private SessionFactory sessionFactory;

    public MessageDAO() {
        try {
            this.sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        } catch (Exception e) {
            StandardServiceRegistryBuilder.destroy(registry);
            System.err.println("Error in hibernate: ");
            e.printStackTrace();

        }
    }

    public void createMessage(Message message){
        try(Session session=sessionFactory.openSession()){
            session.beginTransaction();
            session.save(message);
            session.getTransaction().commit();
        }
    }

    public List<Message> displayMessage(String toUsername){
        List<Message> messages;

        try(Session session=sessionFactory.openSession()){
            Query query=session.createQuery("from Message where toUsername = :user");
            query.setParameter("user",toUsername);
            messages=query.getResultList();
        }

        return messages;

    }
}
