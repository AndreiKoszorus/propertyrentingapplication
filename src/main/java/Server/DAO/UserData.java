package Server.DAO;

import Models.User;

import java.util.List;

/**
 * Created by Admin on 02.05.2018.
 */
public interface UserData {
    public void createUser(User user);


    public List<User> selectUser(String username);


    public void updateUser(User user);


    public void deleteUser(String username);


}
