package Server.DAO;

import Models.Subscription;
import org.hibernate.Session;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Admin on 31.05.2018.
 */
public interface SubData {
    public void createSub(Subscription subscription);

    public void updateSubAdNr(int nr,String username);

    public void updateSubType(boolean type,String username);

    public Subscription getSubscription(String username);
    public void updateLastTimePosted(String username,LocalDateTime date);
}
