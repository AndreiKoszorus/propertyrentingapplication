package Server.DAO;

import Models.Ad;
import Models.FavouriteAds;
import Models.Ratings;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.util.List;

/**
 * Created by Admin on 30.05.2018.
 */
public class RatingDAO implements RatingData {
    private final StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
    private SessionFactory sessionFactory;

    public RatingDAO() {
        try {
            this.sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        } catch (Exception e) {
            StandardServiceRegistryBuilder.destroy(registry);
            System.err.println("Error in hibernate: ");
            e.printStackTrace();

        }
    }

    public void createRating(Ratings ratings){
        try(Session session=sessionFactory.openSession()){
            session.beginTransaction();
            session.save(ratings);
            session.getTransaction().commit();
        }
    }

    public void updateRaiting(int rating,Long id){
        try(Session session=sessionFactory.openSession()) {
            session.beginTransaction();
            javax.persistence.Query query = session.createQuery("from Ratings where adId = :identification");
            query.setParameter("identification", id);
            List<Ratings> ratingsList=query.getResultList();

            for(Ratings updatedRating:ratingsList) {

                updatedRating.setRating(rating);
                session.update(updatedRating);


            }
            session.getTransaction().commit();

        }
    }

    public List<Ratings> findRating(String username,Long id){
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            javax.persistence.Query query = session.createQuery("from Ratings where adId= :identification and username= :name");
            query.setParameter("identification", id);
            query.setParameter("name", username);
            List<Ratings> raitings = query.getResultList();

            return  raitings;


        }
    }
    public List<Ratings> findAdByRate(int rate){
        List<Ratings> ratings;
        try(Session session=sessionFactory.openSession()) {
            javax.persistence.Query query = session.createQuery("from Ratings where rating = :pr");
            query.setParameter("pr", rate);
            ratings = query.getResultList();
        }

        return ratings;
    }

    public void deleteRelatedAds(Long  adId){
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            javax.persistence.Query query = session.createQuery("from Ratings where adId = :identification");
            query.setParameter("identification", adId);
            List<Ratings> ads = query.getResultList();

            for(Ratings ad:ads) {

                session.delete(ad);
            }
            session.getTransaction().commit();


        }
    }

    public List<Ratings> checkRaitingAd(Long id){
        List<Ratings> ratings;
        try(Session session=sessionFactory.openSession()) {
            javax.persistence.Query query = session.createQuery("from Ratings where adId = :pr");
            query.setParameter("pr", id);
            ratings = query.getResultList();
        }

        return ratings;
    }
}
