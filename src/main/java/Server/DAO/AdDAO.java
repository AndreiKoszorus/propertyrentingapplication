package Server.DAO;

import Models.Ad;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.util.List;

/**
 * Created by Admin on 02.05.2018.
 */
public class AdDAO implements AdData {
    private final StandardServiceRegistry registry=new StandardServiceRegistryBuilder().configure().build();
    private SessionFactory sessionFactory;

    public AdDAO(){
        try {
            this.sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        } catch (Exception e) {
            StandardServiceRegistryBuilder.destroy(registry);
            System.err.println("Error in hibernate: ");
            e.printStackTrace();

        }

    }

    public void createAd(Ad ad) {
        try(Session session=sessionFactory.openSession()){
            session.beginTransaction();
            session.save(ad);
            session.getTransaction().commit();
        }
    }

    public List<Ad> selectAds(String owner){
        List<Ad> ads;
        try(Session session=sessionFactory.openSession()) {
            javax.persistence.Query query = session.createQuery("from Ad where owner = :user");
            query.setParameter("user", owner);
            ads = query.getResultList();
        }

        return ads;
    }

    public List<Ad> selectAllAds(){
        List<Ad> ads;
        try(Session session=sessionFactory.openSession()) {
            javax.persistence.Query query = session.createQuery("from Ad");

            ads = query.getResultList();
        }

        return ads;
    }

    public List<Ad> findAdByLocation(String location){
        List<Ad> ads;
        try(Session session=sessionFactory.openSession()) {
            javax.persistence.Query query = session.createQuery("from Ad where location = :loc");
            query.setParameter("loc", location);
            ads = query.getResultList();
        }

        return ads;
    }
    public List<Ad> findAdById(Long id){
        List<Ad> ads;
        try(Session session=sessionFactory.openSession()) {
            javax.persistence.Query query = session.createQuery("from Ad where id = :identification");
            query.setParameter("identification", id);
            ads = query.getResultList();
        }

        return ads;
    }

    public List<Ad> findAdByAvailable(){
        List<Ad> ads;
        try(Session session=sessionFactory.openSession()) {
            javax.persistence.Query query = session.createQuery("from Ad where available = true");
            ads = query.getResultList();
        }

        return ads;
    }

    public List<Ad> findAdByPrice(double price){
        List<Ad> ads;
        try(Session session=sessionFactory.openSession()) {
            javax.persistence.Query query = session.createQuery("from Ad where price = :pr");
            query.setParameter("pr", price);
            ads = query.getResultList();
        }

        return ads;
    }

    public void updateAd(Long id,Ad ad){
        try(Session session=sessionFactory.openSession()) {
            session.beginTransaction();
            javax.persistence.Query query = session.createQuery("from Ad where id = :identification");
            query.setParameter("identification", id);
            List<Ad> ads=query.getResultList();

            Ad updatedAd=ads.get(0);

            updatedAd.setAvailable(ad.isAvailable());
            updatedAd.setLocation(ad.getLocation());
            updatedAd.setOwner(ad.getOwner());
            updatedAd.setPrice(ad.getPrice());
            session.update(updatedAd);
            session.getTransaction().commit();
        }
    }

    public void deleteAd(Long id) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            javax.persistence.Query query = session.createQuery("from Ad where id = :identification");
            query.setParameter("identification", id);
            List<Ad> ads=query.getResultList();

            Ad deleteAd=ads.get(0);

            session.delete(deleteAd);
            session.getTransaction().commit();


        }
    }
}
