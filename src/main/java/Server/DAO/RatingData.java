package Server.DAO;

import Models.Ratings;
import Models.Subscription;
import org.hibernate.Session;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Admin on 30.05.2018.
 */
public interface RatingData {
    public void createRating(Ratings ratings);

    public void updateRaiting(int rating,Long id);
    public List<Ratings> findRating(String username,Long id);
    public List<Ratings> findAdByRate(int rate);
    public void deleteRelatedAds(Long  adId);
    public List<Ratings> checkRaitingAd(Long id);

}
