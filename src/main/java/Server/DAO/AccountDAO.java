package Server.DAO;

import Models.Account;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.util.List;

/**
 * Created by Admin on 02.05.2018.
 */
//Not used in the current project

public class AccountDAO {

    private final StandardServiceRegistry registry=new StandardServiceRegistryBuilder().configure().build();
    private SessionFactory sessionFactory;

    public AccountDAO(){
        try {
            this.sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        } catch (Exception e) {
            StandardServiceRegistryBuilder.destroy(registry);
            System.err.println("Error in hibernate: ");
            e.printStackTrace();

        }

    }

    public void createAccount(Account account) {
        try(Session session=sessionFactory.openSession()){
            session.beginTransaction();
            session.save(account);
            session.getTransaction().commit();
        }
    }

    public Account selectAccount(int accountNumber){
        List<Account> accounts;
        Account account;
        try(Session session=sessionFactory.openSession()) {
            javax.persistence.Query query = session.createQuery("from Account where accountNumber  = :acc");
            query.setParameter("acc", accountNumber);
            accounts = query.getResultList();
            account=accounts.get(0);
        }

        return account;
    }

}
