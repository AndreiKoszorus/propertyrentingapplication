package Server.DAO;

import Models.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.util.List;

/**
 * Created by Admin on 27.04.2018.
 */
public class UserDAO implements UserData {

    private final StandardServiceRegistry registry=new StandardServiceRegistryBuilder().configure().build();
    private SessionFactory sessionFactory;

    public UserDAO(){
        try {
            this.sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        } catch (Exception e) {
            StandardServiceRegistryBuilder.destroy(registry);
            System.err.println("Error in hibernate: ");
            e.printStackTrace();

        }

    }
    public void createUser(User user) {
        try(Session session=sessionFactory.openSession()){
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        }
    }

    public List<User> selectUser(String username){
        List<User> users;
        try(Session session=sessionFactory.openSession()) {
            javax.persistence.Query query = session.createQuery("from User where username = :user");
            query.setParameter("user", username);
           users = query.getResultList();
        }

        return users;



    }

    public void updateUser(User user){
        try(Session session=sessionFactory.openSession()) {
            session.beginTransaction();
            javax.persistence.Query query = session.createQuery("from User where username = :user");
            query.setParameter("user", user.getUsername());
            List<User> users = query.getResultList();
            User updateUser = users.get(0);
            updateUser.setName(user.getName());
            updateUser.setPassword(user.getPassword());
            updateUser.setAdmin(user.isAdmin());
            updateUser.setAccountNumber(user.getAccountNumber());
            session.update(updateUser);
            session.getTransaction().commit();
        }
    }

    public void deleteUser(String username){

        try(Session session=sessionFactory.openSession()) {
            session.beginTransaction();
            javax.persistence.Query query = session.createQuery("from User where username = :user");
            query.setParameter("user", username);
            List<User> users = query.getResultList();
            User deleteUser = users.get(0);
            session.delete(deleteUser);
            session.getTransaction().commit();
        }


    }




}
