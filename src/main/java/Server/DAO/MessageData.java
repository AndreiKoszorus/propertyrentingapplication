package Server.DAO;

import Models.Message;

import java.util.List;

/**
 * Created by Admin on 22.05.2018.
 */
public interface MessageData {
    public void createMessage(Message message);

    public List<Message> displayMessage(String toUsername);
}
