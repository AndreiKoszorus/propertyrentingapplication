package Server.DAO;

import Models.FavouriteAds;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.util.List;

/**
 * Created by Admin on 15.05.2018.
 */
public class FavouriteAdDAO implements FavouriteAdData {
    private final StandardServiceRegistry registry=new StandardServiceRegistryBuilder().configure().build();
    private SessionFactory sessionFactory;

    public FavouriteAdDAO(){
        try {
            this.sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        } catch (Exception e) {
            StandardServiceRegistryBuilder.destroy(registry);
            System.err.println("Error in hibernate: ");
            e.printStackTrace();

        }

    }

    public void createFavouriteAd(FavouriteAds favouriteAds){
        try(Session session=sessionFactory.openSession()){
            session.beginTransaction();
            session.save(favouriteAds);
            session.getTransaction().commit();
        }
    }
    public List<FavouriteAds> selectFavouriteAds(String username){
        List<FavouriteAds> favouriteAds;
        try(Session session=sessionFactory.openSession()){
            javax.persistence.Query query=session.createQuery("from FavouriteAds where username = :user");
            query.setParameter("user",username);
            favouriteAds=query.getResultList();
        }

        return favouriteAds;
    }

    public void deleteFavouriteAd(Long id,String username){
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            javax.persistence.Query query = session.createQuery("from FavouriteAds where adId= :identification and username= :name");
            query.setParameter("identification", id);
            query.setParameter("name", username);
            List<FavouriteAds> ads = query.getResultList();

            FavouriteAds deleteAd = ads.get(0);

            session.delete(deleteAd);
            session.getTransaction().commit();


        }
    }

    public List<FavouriteAds> findById(Long id,String username){
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            javax.persistence.Query query = session.createQuery("from FavouriteAds where adId= :identification and username= :name");
            query.setParameter("identification", id);
            query.setParameter("name", username);
            List<FavouriteAds> ads = query.getResultList();

            return  ads;


        }
    }

    public void deleteRelatedAds(Long id){
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            javax.persistence.Query query = session.createQuery("from FavouriteAds where adId = :identification");
            query.setParameter("identification", id);
            List<FavouriteAds> ads = query.getResultList();

            for(FavouriteAds ad:ads) {

                session.delete(ad);
            }
            session.getTransaction().commit();


        }
    }
}
