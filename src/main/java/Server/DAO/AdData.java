package Server.DAO;

import Models.Ad;

import java.util.List;

/**
 * Created by Admin on 02.05.2018.
 */
public interface AdData {
    public void createAd(Ad ad);

    public List<Ad> selectAds(String owner);

    public List<Ad> findAdByLocation(String location);


    public List<Ad> findAdById(Long id);

    public List<Ad> findAdByPrice(double price);

    public List<Ad> findAdByAvailable();


    public void updateAd(Long id,Ad ad);


    public void deleteAd(Long id);

    public List<Ad> selectAllAds();
}
